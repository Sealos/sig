<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<%@ include file="./head.html"%>
<%@ include file="./header.jsp"%>
<script type="text/javascript">
	$(document).ready(function()
	{   
		var idSoli =  $("#idSol").val();

		$("#cedu").attr("href","${pageContext.request.contextPath}/uploadedDocs/Cedula_"+idSoli+".pdf");
		$("#buenaC").attr("href","${pageContext.request.contextPath}/uploadedDocs/CertBC_"+idSoli+".pdf");
		$("#calif").attr("href","${pageContext.request.contextPath}/uploadedDocs/CalifCert_"+idSoli+".pdf");
		$("#progEst").attr("href","${pageContext.request.contextPath}/uploadedDocs/ProgEst_"+idSoli+".pdf");
		$("#reci").attr("href","${pageContext.request.contextPath}/uploadedDocs/Recibo_"+idSoli+".pdf");
		$("#pict").attr("href","${pageContext.request.contextPath}/uploadedDocs/Foto_"+idSoli+".pdf");
	});
</script>

<s:div id="content">
	<s:form action="aprobarPlanilla" namespace="/" method="POST" align="center">
		<s:hidden id="idSol" value="%{id}" name="nro"/>
		<s:hidden value="1" name="aprobado"/>
		<h2>
		<p><strong>Solicitud de Equivalencia de Estudios</strong></p>
		</h2>
		<s:div id="welcome">
			<p><strong><center>Datos Personales</center></strong></p>
			<div style="width:100%; text-align:center;">
				<div style="float:left; width:50%; border-right: solid 1px #CCC">
				<p>
					<label><b>Apellidos:</b></label>
					<s:property value="%{apellidos}"/>
				</p>
				<p>
					<label><b>Nombres:</b></label>
					<s:property value="%{nombres}"/>
				</p>
				<p>
					<label><b>C.I.:</b></label>
					<s:property value="%{tipoDoc}"/>
					<s:property value="%{cedula}"/>
				</p>
									<p>
					<label><b>Edad:</b></label>
					<s:property value="%{edad}"/>
				</p>
				<p>
					<label><b>Nacionalidad:</b></label>
					<s:property value="%{nacionalidad}"/>
				</p>
				<p>
					<label><b>Pa&iacute;s:</b></label>
					<s:property value="%{pais}"/>
				</p>
				<p>
					<label><b>Fecha Nac:</b></label>
					<s:property value="%{fechanac}"/>
				</p>
				<p>
					<label><b>Nacionalizado:</b></label>
					<s:property value="%{nacionalizado}"/>
				</p>
				<p>
					<label><b>Estado Civil:</b></label>
					<s:property value="%{estadoCiv}"/>
				</p>
				</div>
				<div style="float:right; width:49%;">
				<p>
					<label><b>Sexo:</b></label>
					<s:property value="%{sexo}"/>
				</p>
				<p>
					<label><b>e-mail:</b></label>
					<s:property value="%{email}"/>
				</p>
				<p>
					<label><b>Direcci&oacute;n:</b></label>
					<s:property value="%{direccion}"/>
				</p>
				<p>
					<label><b>Tel&eacute;fono:</b></label>
					<s:property value="%{telefono}"/>
				</p>
				<p>
					<label><b>Direcci&oacute;n 2:</b></label>
					<s:property value="%{direccion2}"/>
				</p>
				<p>
					<label><b>Tel&eacute;fono 2:</b></label>
					<s:property value="%{telefono2}"/>
				</p>
				<p>
					<label><b>Persona a solicitar:</b></label>
					<s:property value="%{personaEsp}"/>
				</p>
				<p>
					<label><b>Nexo:</b></label>
					<s:property value="%{nexo}"/>
				</p>
				</div>
			</div>
		</s:div>

		<s:div id="welcome">
			<p><strong><center>Equivalencias</center></strong></p>
			<s:div>
				<p>
					<label><b>Carrera a solicitar:</b></label>
					<s:property value="%{carrera}"/>
				</p>
			</s:div>
		</s:div>

		<s:div id="welcome">
			<p><strong><center>Documentos de Equivalencia</center></strong></p>
			<div style="width:100%; text-align:center;">
				<div style="float:left; width:50%; border-right: solid 1px #CCC">
				<p>
					<a id="cedu" target="_blank"><img border="0" src="${pageContext.request.contextPath}/images/pdf.gif"></a>Cedula</a>
				</p>
				<p>
					<a id="buenaC" target="_blank"><img border="0" src="${pageContext.request.contextPath}/images/pdf.gif"></a>Certificado Buena Conducta</a>
				</p>
				<p>
					<a id="calif" target="_blank"><img border="0" src="${pageContext.request.contextPath}/images/pdf.gif"></a>Calificaciones Certificadas</a>
				</p>
				</div>
				<div style="float:right; width:49%;">
				<p>
					<a id="progEst" target="_blank"><img border="0" src="${pageContext.request.contextPath}/images/pdf.gif"></a>Programa de Estudios</a>
				</p>
				<p>
					<a id="reci" target="_blank"><img border="0" src="${pageContext.request.contextPath}/images/pdf.gif"></a>Recibo</a>
				</p>
				<p>
					<a id="pict" target="_blank"><img border="0" src="${pageContext.request.contextPath}/images/pdf.gif"></a>Foto</a>
				</p>
				</div>
			</div>
		</s:div>
		<center>
			<input type="submit" name="Submit" value="Aprobar" onclick=""/>

		</center>
	</s:form>
	<s:form action="aprobarPlanilla" namespace="/" method="POST" align="center">
		<s:hidden value="%{id}" name="nro"/>
		<s:hidden value="0" name="aprobado"/>
		<input type="submit" name="Submit" value="Rechazar" onclick=""/>
	</s:form>
		
</s:div>
<%@ include file="./footer.html"%>