<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<%@ include file="./head.html"%>
<%@ include file="./header.jsp"%>
		<h2 style="text-align:center; font-size: 30px"> Consulta de las Tablas de Equivalencias</h2>
		<br />
		<br />
		<center>
		<input type="text" name="nomTabla" id="nomTabla"/>
		<input type="button" name="botonBuscar" id="botonBuscar" value="Buscar"/>
		<s:form action="/consultarTabla" theme="simple">
			Tabla de Equivalencia:
			<s:select name="codTab" id="selectTab" list="tablaList"/>
			<br><br>
			
			<div style="width:100%; text-align:center;"> <!-- Main Div -->
				<div style="float:left; width:50%; ">
					<h1 style="text-align:center; font-size: 15px">
						Universidad de Origen
					</h1> <br />
					Institucion:<br />
						<input id="sUniSrc" type="text" name="sUniSrc" required = "true" disabled/>
						<br><br>
						
					Carrera:<br />
						<input id="sCarreraSrc" type="text" name="sCarreraSrc" required = "true" disabled/>
					<br><br>

					Materia:<br />
						<input id="sMatSrc1" type="text" name="sMatSrc1" required = "true" disabled/>
					<br><br>

					Materia:<br />
						<input id="sMatSrc2"type="text" name="sMatSrc2" required = "true" disabled/>
					<br><br>

					Materia:<br />
						<input id="sMatSrc3" type="text" name="sMatSrc3" required = "true" disabled/>
					<br><br>

				</div>
                    
                    <div style="float:right; width:50%;">
						<h1 style="text-align:center; font-size: 15px">
							Universidad de Destino
						</h1> <br />
						Institucion:<br />
							<input id="sUniDst" type="text" name="sUniDst" required = "true" disabled/>
						<br><br>
							
						Carrera:<br />
							<input id="sCarreraDst" type="text" name="sCarreraDst" required = "true" disabled/>
						<br><br>

						Materia:<br />
							<input id="sMatDst1" type="text" name="sMatDst1" required = "true" disabled/>
						<br><br>

                                                Materia:<br />
							<input id="sMatDst2" type="text" name="sMatDst2" required = "true" disabled/>
						<br><br>

                                                Materia:<br />
							<input id="sMatDst3" type="text" name="sMatDst3" required = "true" disabled/>
						<br><br>

                    </div>
                </div>

		</s:form>
		</center>
<%@ include file="./footer.html"%>