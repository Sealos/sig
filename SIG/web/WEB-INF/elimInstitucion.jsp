<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<%@ include file="./head.html"%>
<%@ include file="./header.jsp"%>

		<h2 style="text-align:center; font-size: 30px"> Eliminar Institución

		<br><br>
		<span id="msjOut"><s:property value="#request.mensaje" /></span>
		</h2>
		<center>
		<s:form onsubmit="return confirmacionEliminar();" action="/elimiInstitucion" theme="simple">
			Institucion:<br />
			<s:select name="acronimo" list="%{instituciones}" id ="selectIns"/><br />
				 <br><br>
			<s:submit align="center" value="Eliminar" />
		</s:form>
		<br>
		</center>
<%@ include file="./footer.html"%>
