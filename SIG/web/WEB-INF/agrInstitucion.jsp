<%@page contentType="text/html, java" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<%@ include file="./head.html"%>
<%@ include file="./header.jsp"%>
		<h2 style="text-align:center; font-size: 30px"> Agregar Institucion
		<br><br>
		<span id="msjOut"><s:property value="#request.mensaje" /></span>
		</h2>

		<center>
		<s:form action="/agrInstitucion" theme="simple">
				Nombre: <br /><input type="text" name="sNuevoNombre" required = "true" size="64"/><br />
				<br>
				Acrónimo: <br /><input type="text" name="sNuevoAcronimo" required = "true"/><br />
				<br>
				Dirección: <br /><input type="text" name="sNuevoDir" required = "true"/><br />
				<br>
				País:<br />
				<%@ include file="./paises.html"%>
				<br>
				<br>
				<s:submit align="center" value="Agregar a la base de datos" />

		</s:form>	<br />
		<br>
		</center>
<%@ include file="./footer.html"%>
