<%@ taglib prefix="s" uri="/struts-tags" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://www.usb.ve/equivalencias"
		http-equiv="Content-Style-Type"
		content="text/css">
	<meta http-equiv="Content-Type"
		  content="text/html; charset=utf-8" />
	<meta charset="utf-8" />
	<link rel="shortcut icon"
		  href="http://www.dace.usb.ve/sites/default/files/bluemarine_favicon.ico"
		  type="image/vnd.microsoft.icon" />

	<title>Sistema de Gesti�n de Equivalencias</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/jquery-ui.css" type="text/css" media="all" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/solicitud.css" type="text/css" media="all" />

	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-ui.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/select.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/selectAjax.js"></script>
	<link rel="shortcut icon" href="http://www.dace.usb.ve/sites/default/files/bluemarine_favicon.ico" type="image/vnd.microsoft.icon" />
	<script>
		$(function()
		{
			 $("#datepicker").datepicker({ dateFormat: "dd/mm/yy" }).val();
		});

		$.ajax(
			{
				type: "POST",
				url: "cargarCarreras?uni="+1,
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(json)
				{
					$('#selectCar').append('<option value=0>Seleccione Carrera</option>');
					$.each(json, function(i, value)
					{
						$.each(value, function(j, index)
						{
							$('#selectCar').append($('<option>').text(j).attr("value",index));
						});
					});
				}
			});

	</script>
</head>
	<body>
	<s:div id="header">
		<table border="0" width="100%">
		<tr>
			<td>
				<img src="./images/usb-logo.gif" alt="USB" />
			</td>
			<td align="center">
				<h1>Universidad Sim&oacute;n Bol&iacute;var</h1>
				<h2>Sistema de Gesti�n de Equivalencias</h2>
			</td>
		</tr>
		</table>
	</s:div>

	<s:div id="content">
		<s:div id="errors">
			<ul>
				<s:actionerror/>
			</ul>
		</s:div>

		<s:form action="agregarSolicitud" namespace="/" method="POST" align="center" enctype="multipart/form-data" onsubmit="return confirmacion();">
			<h2>
			<p><strong>Solicitud de Equivalencia de Estudios</strong></p>
			</h2>
			<s:div id="welcome">
				<p><strong>Datos Personales</strong></p>
				<s:div id="contenido">
					<p>
						<label>Apellidos:</label>
						<input type="text" name="apellido" size="18" id="apellido" required="true"/>
						<label>Nombres:</label>
						<input type="text" name="nombre" size="18" id="nombre" required="true"/>

						<label for='cedula'>C.I.:</label>
						<select name="nac">
							<option value="V">V</option>
							<option value="E">E</option>
						</select>
						<input type="text" name="cedula" size="17" required="true" id="cedula"/>
					</p>
					<p>
						<label>Nacionalidad:</label>
						<input type="text" name="nacionalidad" size="16" required="true" id="nacionalidad"/>

						<label>Pa&iacute;s</label>
						<%@ include file="./paises.html"%>
						<label>Fecha Nac</label>
						<input type="text" name="datepicker" size="6" id="datepicker"/>
					</p>
					<p>
						<label>Nacionalizado</label><input type="checkbox" name="nacionalizado" value="nacionalizado">
							<label>Estado Civil</label>
						<select name="estado">
							<option value="Soltero">Soltero</option>
							<option value="Casado">Casado</option>
							<option value="Viudo">Viudo</option>
							<option value="Divorciado">Divorciado</option>
						</select>
						<label>Sexo</label>
						<select name="sexo">
							<option value="M">M</option>
							<option value="F">F</option>
						</select>
						<label>e-mail:</label>
						<input type="text" name="email" size="34" required="true" id="email"/>
					</p>
					<p>
						<label>Direcci&oacute;n</label>
						<input type="text" name="direccion" size="62" required="true" id="direccion"/>
						<label>Tel&eacute;fono</label>
						<input type="text" name="telefono" size="12" required="true" id="telefono"/>
					</p>
					<p>
						<label>Direcci&oacute;n 2</label>
						<input type="text" name="direccion2" size="60" required="true" id="direccion2"/>
						<label>Tel&eacute;fono</label>
						<input type="text" name="telefono2" size="12" required="true" id="telefono2"/>
					</p>
					<p>
						<label>Persona a solicitar</label>
						<input type="text" name="personaEsp" size="50" required="true" id="personaEsp"/>
						<label>Nexo</label>
						<input type="text" name="nexo" size="18" required="true" id="nexo"/>
					</p>
				</s:div>
			</s:div>

			<div id="welcome">
				<p><strong>Documentos de Equivalencias</strong></p>
				<div style="width:100%; text-align:center;">
					<div style="float:left; width:50%; border-right: solid 1px #CCC">
						<p>
							<label for="fileUpload">C&eacute;dula</label>
						</p>
						<p>
							<input type=file name="fileUpload" id="fileUpload" size="40"/>
						</p>
						<p>
							<label>Certificado Buena Conducta</label>
						</p>
						<p>
							<input type=file name="fileUpload" size="40"/>
						</p>
						<p>
							<label>Calificaciones Certificadas</label>
						</p>
						<p>
							<input type=file name="fileUpload" size="40"/>
						</p>
					</div>
					<div style="float:right; width:49%;">
						<p>
							<label for="fileUpload">Programa de Estudios</label>
						</p>
						<p>
							<input type=file name="fileUpload" id="fileUpload" size="40"/>
						</p>
						<p>
							<label>Recibo</label>
						</p>
						<p>
							<input type=file name="fileUpload" size="40"/>
						</p>
						<p>
							<label>Foto</label>
						</p>
						<p>
							<input type=file name="fileUpload" size="40"/>
						</p>
					</div>
				</div>
			</div>
			<div id="welcome">
				<p><strong>Equivalencias</strong></p>
				<label>Carrera de la USB a solicitar:</label><br>
						<select name="codCar" id="selectCar" list="carreraList">
			</div>
			<input type="submit" name="Submit" value="Enviar Solicitud"/>
		</s:form>
		<s:form action="index" namespace="/" method="POST" align="center">
			<input type="submit" name="Submit" value="Regresar" />
		</s:form>
	</s:div>

<%@ include file="./footer.html"%>