<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<%@ include file="./head.html"%>
<%@ include file="./header.jsp"%>
		<h2 style="text-align:center; font-size: 30px"> Modificar Tabla de Equivalencia
			<br><br>
			<span id="msjOut"><s:property value="#request.mensaje" /></span>
		</h2>
		<input value="1" hidden="true">
		<br />
		<br />
		<center>
			<input type="text" name="nomTabla" id="nomTabla"/>
			<input type="button" name="botonBuscar" id="botonBuscar" value="Buscar"/>
		</center>
			<s:form action="/modTabla" theme="simple" onsubmit="return confirmarTabla();">
				<s:hidden name="val" value="1"/>
				<center>
					Tabla de Equivalencia:
					<s:select name="codTab" id="selectTab" list="tablaList"/>
				</center>
				<br>
				<br>
				<br>
                <div style="width:100%; text-align:center;"> <!-- Main Div -->
                    <div style="float:left; width:50%; ">
                        <h1 style="text-align:center; font-size: 15px">
							Universidad de Origen
						</h1> <br />
						Institucion:<br />
							<s:select  name="acronimo" key="nombre" id="selectIns" list="institucionList" value="acronimo" headerKey="0" headerValue="Seleccione Institucion"
				   listValue="%{nombre}" listKey="%{acronimo}"	/>
                            <br><br>
							Carrera:<br />

                                <s:select name="codCar" id="selectCar" list="carreraList"/>

                            <br><br>

                            Materia:<br />

                            	<s:select name="codMat" id="selectMat" list="carreraList" />

                            <br><br>

                            Materia:<br />
                            	<s:select name="codMat2" id="selectMat2" list="carreraList" />
                            <br><br>

                            Materia:<br />
                            	<s:select name="codMat3" id="selectMat3" list="carreraList" />
                            <br><br>

                    </div>
                    
                    <div style="float:right; width:50%;">
						<h1 style="text-align:center; font-size: 15px">
							Universidad de Destino
						</h1> <br />
						Institucion:<br />
							<s:select  name="acronimo2" key="nombre2" id="selectIns2" list="institucionList" value="acronimo2" headerKey="0" headerValue="Seleccione Institucion"
				   listValue="%{nombre}" listKey="%{acronimo}"	/>
                            <br><br>
							Carrera:<br />

                                <s:select name="codCar2" id="selectCar2" list="carreraList"/>

                            <br><br>

                            Materia:<br />

                            	<s:select name="codMat4" id="selectMat4" list="carreraList" />

                            <br><br>

                            Materia:<br />
                            	<s:select name="codMat5" id="selectMat5" list="carreraList" />
                            <br><br>

                            Materia:<br />
                            	<s:select name="codMat6" id="selectMat6" list="carreraList" />
                            <br><br>
                    </div>
                </div>
				<center>
		
				<div style="position:absolute; top:780px; right:60%; left:48%">
					
					<s:submit align="center" value="Modificar"/>
				</div>
				</center>
			</s:form>
			<div style="clear:both"></div>
			

<%@ include file="./footer.html"%>