<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<%@ include file="./head.html"%>
<%@ include file="./header.jsp"%>
<style>
	#this a {
		background:#fff;
		color: #000;
	}
	#this a:hover {
		background:#fff;
	}
	#this td
	{
		text-align:center; 
		vertical-align:middle;
	}
	#this tr:first-child {background: #5f6975}
	#this tr:nth-child(2n+3) {background: #efefef}
	#this tr:nth-child(2n+3) a {background: #efefef!important}

	#this2 a {
		background:#fff;
		color: #000;
	}
	#this2 a:hover {
		background:#fff;
	}
	#this2 td
	{
		text-align:center; 
		vertical-align:middle;
	}
	#this2 tr:first-child {background: #5f6975}
	#this2 tr:nth-child(2n+3) {background: #efefef}
	#this2 tr:nth-child(2n+3) a {background: #efefef!important}
	#this3 a {
		background:#fff;
		color: #000;
	}
	#this3 a:hover {
		background:#fff;
	}
	#this3 td
	{
		text-align:center; 
		vertical-align:middle;
	}
	#this3 tr:first-child {background: #5f6975}
	#this3 tr:nth-child(2n+3) {background: #efefef}
	#this3 tr:nth-child(2n+3) a {background: #efefef!important}
	th
	{
		color: #FFF;
	}
	#logo tr {background: #FFF!important}
</style>
<center>

	<div id="this">
		<s:if test="%{solicitudes.size != 0}">
			<h2 style="text-align:center; font-size: 20px">
				Solicitudes Pendientes
			</h2>
			<table width="28%" cellpadding="4" cellspacing="0" border="0">
				<tr align="center">
				<th>Nro Solicitud</th>
				<th>Solicitud</th>
				<th>Fecha de Ingreso</th>
				</tr>
				<s:iterator value="solicitudes">
				<tr>
					<td>
						<s:property value="%{nroexpediente}"/>
					</td>
					<td>
						<s:url var="myUrl" action="consultarPlanilla">
							<s:param name="id"><s:property value="%{nroexpediente}"/></s:param>
						</s:url>

						<s:a href="%{#myUrl}"><s:property value="%{nombre}"/></s:a>
					</td>
					<td>
						<s:property value="%{fecha}"/>
					</td>
				</tr>
				</s:iterator>
			</table>
		</s:if>
		<s:else>
			No hay solicitudes pendientes
		</s:else>
	</div>
	<br>
	<div id="this2">
		<s:if test="%{solicitudesAprobadas.size != 0}">
			<h2 style="text-align:center; font-size: 20px">
				Solicitudes Aprobadas
			</h2>
			<table width="28%" cellpadding="4" cellspacing="0" border="0">
				<tr align="center">
				<th>Nro Solicitud</th>
				<th>Solicitud</th>
				<th>Fecha de Ingreso</th>
				</tr>
				<s:iterator value="solicitudesAprobadas">
				<tr>
					<td>
						<s:property value="%{nroexpediente}"/>
					</td>
					<td>
						<s:url var="myUrl" action="consultarPlanilla">
							<s:param name="id"><s:property value="%{nroexpediente}"/></s:param>
						</s:url>

						<s:a href="%{#myUrl}"><s:property value="%{nombre}"/></s:a>
					</td>
					<td>
						<s:property value="%{fecha}"/>
					</td>
				</tr>
				</s:iterator>
			</table>
		</s:if>
		<s:else>
			No hay solicitudes aprobadas
		</s:else>
	</div>
	<br>
	<div id="this3">
		<s:if test="%{solicitudesRechazadas.size != 0}">
			<h2 style="text-align:center; font-size: 20px">
				Solicitudes Rechazadas
			</h2>
			<table width="28%" cellpadding="4" cellspacing="0" border="0">
				<tr align="center">
				<th>Nro Solicitud</th>
				<th>Solicitud</th>
				<th>Fecha de Ingreso</th>
				<th>Eliminar</th>
				</tr>
				<s:iterator value="solicitudesRechazadas">
				<tr>
					<td>
						<s:property value="%{nroexpediente}"/>
					</td>
					<td>
						<s:url var="myUrl" action="consultarPlanilla">
							<s:param name="id"><s:property value="%{nroexpediente}"/></s:param>
						</s:url>

						<s:a href="%{#myUrl}"><s:property value="%{nombre}"/></s:a>
					</td>
					<td>
						<s:property value="%{fecha}"/>
					</td>
					<td>
						<s:url var="myUrl" action="eliminarPlanilla">
							<s:param name="id"><s:property value="%{nroexpediente}"/></s:param>
						</s:url>
						<s:a href="%{#myUrl}"><img border="0" src="${pageContext.request.contextPath}/images/delete.gif"></s:a>
					</td>
				</tr>
				</s:iterator>
			</table>
		</s:if>
		<s:else>
			No hay solicitudes rechazadas
		</s:else>
	</div>

		
</center>
<%@include file="./footer.html"%>
