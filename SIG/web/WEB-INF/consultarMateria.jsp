<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<%@ include file="./head.html"%>
<%@ include file="./header.jsp"%>
<h2 style="text-align:center; font-size: 30px">Consultar Materia
	<br><br>
	<span id="msjOut"><s:property value="#request.mensaje" /></span>
</h2>
<center>
	<s:form action="" theme="simple">
		Institucion:<br />
		 
			 <s:select  name="acronimo" key="nombre" id="selectIns" list="institucionList" value="acronimo" headerKey="0" headerValue="Seleccione Institucion"
					   listValue="%{nombre}" listKey="%{acronimo}"	/>
		 
		<br><br>
		Carrera:<br />
			<s:select name="codCar" id="selectCar" list="carreraList"/>
		<br><br>

		Materia:<br />
			<s:select name="codMat" id="selectMat" list="carreraList" />
		<br><br>

		C&oacute;digo: <br /><input id="sNuevoCodigoMat" type="text" name="sNuevoCodigoMat" required = "true" disabled/><br /><br />
		Nombre: <br /><input id="sNuevoNombreMat" type="text" name="sNuevoNombreMat" required = "true" disabled/><br /><br />
		Cr&eacute;ditos: <br /><input id="sNuevoCreditosMat" type="text" name="sNuevoCreditosMat" required = "true" disabled/>
	</s:form>
</center>
<%@ include file="./footer.html"%>
