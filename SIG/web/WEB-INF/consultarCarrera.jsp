<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<%@ include file="./head.html"%>
<%@ include file="./header.jsp"%>
<h2 style="text-align:center; font-size: 30px"> Consultar Carrera

<br><br>
</h2>
<center>
	<s:form action="" theme="simple">
		Institucion:<br />
		 <s:select  name="acronimo" key="nombre" id="selectIns" list="institucionList" value="acronimo" headerKey="0" headerValue="Seleccione Institucion"
					   listValue="%{nombre}" listKey="%{acronimo}"	/>
		<br><br>
		Carrera:<br />
		<s:select name="codCar" id="selectCar" list="carreraList"/>
		<br><br>

		C&oacute;digo: <br /><input id="sNuevoCodigoCar" type="text" name="sNuevoCodigo" required = "true" disabled/><br /><br />
		Nombre: <br /><input id="sNuevoNombreCar" type="text" name="sNuevoNombre" required = "true" disabled/>
	</s:form>
</center>
<%@ include file="./footer.html"%>
