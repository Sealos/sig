<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<%@ include file="./head.html"%>
<%@ include file="./header.jsp"%>
		<h2 style="text-align:center; font-size: 30px"> Eliminar Tabla de Equivalencia
		<br><br>
		<span id="msjOut"><s:property value="#request.mensaje" /></span>
		</h2>
		<center>
		<s:form onsubmit="return confirmacionEliminar();" action="/elimiTabla" theme="simple">
		<input type="text" name="nomTabla" id="nomTabla"/>
		<input type="button" name="botonBuscar" id="botonBuscar" value="Buscar"/>
		<br>
			Tabla de Equivalencia:
			<s:select name="codTab" id="selectTab" list="tablaList"/>
			<br><br>
			<s:submit align="center" value="Eliminar" onsubmit="return confirmacionEliminar();" />
		</s:form>
		<br>
		</center>
<%@ include file="./footer.html"%>
