<%@ taglib prefix="s" uri="/struts-tags" %>
<body>
	<s:div id="header">
		<table border="0" width="100%" id="logo">
		<tr>
			<td>
				<img src="./images/usb-logo.gif" alt="USB" />
			</td>
			<td align="center">
				<h1>Universidad Sim&oacute;n Bol&iacute;var</h1>
				<h2>Sistema de Gesti&oacute;n de Equivalencias</h2>
			</td>
		</tr>
		</table>
	</s:div>
	<center>
		<nav align="center">
			<ul>
				<li><a href="<s:url action="index"/>">Inicio</a></li>
				<s:if test="%{#session.Login.permiso & 2}">
					<li><a href="<s:url action="consultarSolicitudes"/>">Consultar Solicitudes</a></li>
					<li><a href="#">Gestionar Expedientes</a></li>
				</s:if>
				<s:if test="%{#session.Login.permiso & 8}">
					<li><a>Gestionar Instituciones</a>
						<ul>
							<li><a href="<s:url action="consultarInstitucion"/>">Consultar Instituci&oacute;n</a>
							<li><a href="<s:url action="agrInstitucion"/>">Agregar Instituci&oacute;n</a></li>
							<li><a href="<s:url action="modInstitucion"/>">Modificar Instituci&oacute;n</a></li>
							<li><a href="<s:url action="elimInstitucion"/>">Eliminar Instituci&oacute;n</a></li>
						</ul>
					</li>
					<li><a>Gestionar Carreras</a>
						<ul>
							<li><a href="<s:url action="consultarCarrera"/>">Consultar Carrera</a>
							<li><a href="<s:url action="agregarCarrera"/>">Agregar Carrera</a></li>
							<li><a href="<s:url action="modCarrera"/>">Modificar Carrera</a></li>
							<li><a href="<s:url action="elimCarrera"/>">Eliminar Carrera</a></li>
						</ul>
					</li>
					<li><a>Gestionar Materias</a>
						<ul>
							<li><a href="<s:url action="consultarMateria"/>">Consultar Materia</a>
							<li><a href="<s:url action="agregarMateria"/>">Agregar Materia</a>
							<li><a href="<s:url action="modificarMateria"/>">Modificar Materia</a>
							<li><a href="<s:url action="elimMateria"/>">Eliminar Materia</a>
						</ul>
					</li>
						<li><a>Gestionar Tablas</a>
						<ul>
							<li><a href="<s:url action="consultarTabla"/>">Consultar Tabla</a>
							<li><a href="<s:url action="agregarTabla"/>">Agregar Tabla</a>
							<li><a href="<s:url action="modTablasEquiv"/>">Modificar Tabla</a>
							<li><a href="<s:url action="elimTablasEquiv"/>">Eliminar Tabla</a>
						</ul>
					</li>
					</s:if>
				<li><a href="<s:url action="Logout"/>">Cerrar Sesi&oacute;n</a></li>
			</ul>
		</nav>
	</center>