<%@page contentType="text/html, java" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<%@ include file="./head.html"%>
<%@ include file="./header.jsp"%>

		<h2 style="text-align:center; font-size: 30px"> Modificar Institucion


		<br><br>
		<span id="msjOut"><s:property value="#request.mensaje" /></span>
		</h2>

		<center>
		<s:form action="/modiInstitucion" theme="simple" onsubmit="return confirmacion();">
			Institucion:<br />
			<s:select name="acronimo" list="%{instituciones}" id ="selectIns"/><br />
				<br>
                                Nombre: <br /><input id="sNuevoNombre" type="text" name="sNuevoNombre" required = "true" size="64"/><br />
				<br>
				Acrónimo: <br /><input id="sNuevoAcronimo" type="text" name="sNuevoAcronimo" required = "true"/><br />
				<br>
				Dirección: <br /><input id="sNuevoDir" type="text" name="sNuevoDir" required = "true"/><br />
				<br>
				País:<br />
				<%@ include file="./paises.html"%>
				<br>
				<br>
				<s:submit align="center" value="Modificar" />

		</s:form>	<br />
		<br>
		</center>
<%@ include file="./footer.html"%>
