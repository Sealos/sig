<%@page contentType="text/html, java" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<%@ include file="./head.html"%>
<%@ include file="./header.jsp"%>

<h2 style="text-align:center; font-size: 30px">
	Consultar Institucion
</h2>

<center>
	<s:form action="" theme="simple">
		Institucion:<br />
			<s:select name="acronimo" list="%{instituciones}" id ="selectIns"/><br />
			<br>
			Nombre: <br /><input id="sNuevoNombre" type="text" name="sNuevoNombre" required = "true" disabled size="64"/><br />
			<br>
			Acrónimo: <br /><input id="sNuevoAcronimo" type="text" name="sNuevoAcronimo" required = "true" disabled/><br />
			<br>
			Dirección: <br /><input id="sNuevoDir" type="text" name="sNuevoDir" required = "true" disabled/><br />
			<br>
			País: <br /><input id="sNuevoPais" type="text" name="sNuevoPais" required = "true" disabled/><br />
	</s:form>
</center>
<%@ include file="./footer.html"%>
