<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<%@ include file="./head.html"%>
<%@ include file="./header.jsp"%>
<h1 style="text-align:center; font-size: 30px"> Agregar Carrera
	<br><br>
	<span id="msjOut"><s:property value="#request.mensaje" /></span>
</h1>

<center>
	<s:form action="agrCarrera" theme="simple" onsubmit="return confirmacion();">
		<br />
		<br>
		Seleccione la institucion:<br />
		
			<s:select  name="acronimo" key="nombre" id="selectIns" list="institucionList" value="acronimo" headerKey="0" headerValue="Seleccione Institucion"
		   listValue="%{nombre}" listKey="%{acronimo}" />
		<br /><br />
		Ingrese el codigo de la carrera: <br /><input type="text" name="sCodigo" required = "true"/><br /><br />
		Ingrese el nombre de la carrera: <br /><input type="text" name="sNombre" required = "true"/><br /><br />
		<br><br />
		<s:submit align="center" value="Aceptar" />
	</s:form>	<br /><br />
	<br>
</center>
<%@ include file="./footer.html"%>
