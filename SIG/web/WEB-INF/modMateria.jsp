<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<%@ include file="./head.html"%>
<%@ include file="./header.jsp"%>
<h2 style="text-align:center; font-size: 30px">Modificar Materia
	<br><br>
	<span id="msjOut"><s:property value="#request.mensaje" /></span>
</h2>
<center>
	<s:form action="/modiMateria" theme="simple" onsubmit="return confirmacion();">
		Institucion:<br />
		 
			 <s:select  name="acronimo" key="nombre" id="selectIns" list="institucionList" value="acronimo" headerKey="0" headerValue="Seleccione Institucion"
					   listValue="%{nombre}" listKey="%{acronimo}"	/>
		 
		<br><br>
		Carrera:<br />
			<s:select name="codCar" id="selectCar" list="carreraList"/>
		<br><br>

		Materia:<br />
			<s:select name="codMat" id="selectMat" list="carreraList" />
		<br><br>

		Ingrese el nombre de la materia:<br />
		<input type="text" name="sNuevoNombre" required = "true"/>
		<br><br>
		<s:submit align="center" value="Aceptar" />
	</s:form>

</center>
<%@ include file="./footer.html"%>
