<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<%@ include file="./head.html"%>
<%@ include file="./header.jsp"%>

		<h1 style="text-align:center; font-size: 30px"> Agregar Materia
                    

		<br><br>
		<span id="msjOut"><s:property value="#request.mensaje" /></span>

		</h1>
		<center>
		<s:form action="/agrMateria" theme="simple" onsubmit="return confirmacion();">
		<br />
                
                Seleccione la institucion:<br />
				<s:select  name="acronimo" key="nombre" id="selectIns" list="institucionList" value="acronimo" headerKey="0" headerValue="Seleccione Institucion"
						   listValue="%{nombre}" listKey="%{acronimo}"  />
				<br><br />
                Seleccione la Carrera:<br />
                <s:select name="codCar" id="selectCar" list="carreraList"/>
				<br><br />
				Ingrese el codigo de la materia: <br /><input type="text" name="sNuevoCodigo" required = "true"/><br /><br />
				Ingrese el nombre de la materia: <br /><input type="text" name="sNuevoNombre" required = "true"/><br /><br />
				Ingrese el numero de creditos: <br /><input type="text" name="sNuevoCreditos" required = "true"/><br /><br />
				<span hidden="" id="codCoord"><s:property value="#session.Cod" /></span>
           
      
				<s:submit align="center" value="Agregar Materia" />

		</s:form>
		<br /><br />
		<br>
		</center>

<%@ include file="./footer.html"%>