	function trim(s)
	{
		return s.replace( /^\s*/, "" ).replace( /\s*$/, "" );
	}

	function verificarCombos()
	{
		var combos = ["acronimo", "codCar", "codMat", "acronimo2", "codCar2", "codMat4", "codTab"];

		for (var i = 0; i < combos.length; i++)
		{
			var els = document.getElementsByName(combos[i]);
			for (var j = 0; j < els.length; j++)
			{
				var an_element = els[j];
				if (an_element.selectedIndex == 0 || an_element.selectedIndex == -1)
					return false;
			}
		}
		return true;
	}

	function verificarUploads()
	{
		var uploads = document.getElementsByName("fileUpload");
		for (var i = 0; i < uploads.length; i++)
		{
			if (uploads[i].value == "")
				return false;
		}
		return true;
	}

	function checkTabla()
	{
		var combos = [["codMat", "codMat2", "codMat3"],["codMat4","codMat5","codMat6"]];
		combos[0][0] = document.getElementsByName(combos[0][0])[0].selectedIndex;
		combos[0][1] = document.getElementsByName(combos[0][1])[0].selectedIndex;
		combos[0][2] = document.getElementsByName(combos[0][2])[0].selectedIndex;

		combos[1][0] = document.getElementsByName(combos[1][0])[0].selectedIndex;
		combos[1][1] = document.getElementsByName(combos[1][1])[0].selectedIndex;
		combos[1][2] = document.getElementsByName(combos[1][2])[0].selectedIndex;

		for (var i = 0; i < combos.length; i++)
			{
				for (var j = 0; j < combos[i].length; j++)
				{
					for (var k = j+1; k < combos[i].length; k++)
					{
						if (combos[i][j] != 0 && combos[i][j] != -1)
						{
							if (combos[i][j] === combos[i][k])
								return false;
						}
					}
				}
			}
		return true;
	}

	function checkUniversidades()
	{
		var combos = ["acronimo","acronimo2","codCar","codCar2"];
		var uni1 = document.getElementsByName(combos[0])[0].selectedIndex;
		var uni2 = document.getElementsByName(combos[1])[0].selectedIndex;
		var car1 = document.getElementsByName(combos[2])[0].selectedIndex;
		var car2 = document.getElementsByName(combos[3])[0].selectedIndex;

		if (uni1 === uni2 && car1 === car2)
			return false;
		else
			return true;
	}

	function confirmarTabla()
	{
		if (!verificarCombos())
		{
			alert("Verifique que no hay menus sin seleccionar");
			return false;
		}
		else
		{
			if (checkUniversidades())
			{
				if (checkTabla())
					return true;
				else
				{
					alert("Se encontraron materias duplicadas");
					return false;
				}
			}
			else
			{
				alert("No se puede hacer equivalencias\nentre la misma carrera de la\nmisma universidad");
				return false;
			}
		}
	}

	function confirmacionEliminar()
	{
		if (!verificarCombos())
		{
			alert("Verifique que no hay menus sin seleccionar");
			return false;
		}
		else
		{
			try
			{
				var c = confirm("Note que pueden existir datos asociados que se pueden perder\n¿Esta seguro de que desea eliminar?");
				return c;
			}
			catch (e)
			{
				alert("Se produjo un error, intente de nuevo por favor");
			}
		}
	}

	function confirmacion()
	{
		var b = verificarCombos() && verificarUploads();
		if (!b)
		{
			var sms = "";
			if (!verificarCombos())
				sms = sms + "Verifique que no hay menus sin seleccionar"
			if (!verificarUploads())
				sms = sms + "\n"+"Verifique que no le faltan archivos por subir"

			alert(sms);
			return false;
		}
		else
			return true;
	}

	$(document).ready(function()
	{
		var uni;
		var uni2;
		var uni3;
		var carr;
		var tab;
		var mat;
		$("#botonBuscar").on('click',function()
		{
			var tabB = $('#nomTabla').val();
			$('#selectTab')[0].options.length = 0;
			$.ajax(
			{
				type: "POST",
				url: "cargarTablas?tab="+tabB,
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(json)
				{
					$('#selectTab').append('<option value=1>Seleccione Tabla de Equivalencia</option>');
					$.each(json, function(i, value)
					{
						$.each(value, function(j, index)
						{
							$('#selectTab').append($('<option>').text(j).attr("value",index));
						});
					});
				}
			});
		})

		$("#msjOut").fadeIn(2000).delay(500).fadeOut(5000);
		$("#selectIns").change(function()
		{

			uni = $("#selectIns").find(':selected').val();
            cargarDatos();
			$('#selectCar')[0].options.length = 0;

			$.ajax(
			{
				type: "POST",
				url: "cargarCarreras?uni="+uni,
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(json)
				{
					$('#selectCar').append('<option value=1>Seleccione Carrera</option>');
					$.each(json, function(i, value)
					{
						$.each(value, function(j, index)
						{
							$('#selectCar').append($('<option>').text(j).attr("value",index));
						});
					});

				}
			});
		});
		$("#selectIns2").change(function()
		{
			uni = $("#selectIns2").find(':selected').val();
			$('#selectCar2')[0].options.length = 0;
			$.ajax(
			{
				type: "POST",
				url: "cargarCarreras?uni="+uni,
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(json)
				{
					$('#selectCar2').append('<option value=1>Seleccione Carrera</option>');
					$.each(json, function(i, value)
					{
						$.each(value, function(j, index)
						{
							$('#selectCar2').append($('<option>').text(j).attr("value",index));
						});
					});

				}
			});
		});

		function cargarDatos()
		{
			$.ajax
			({
				type: "POST",
				url: "rellenarInputs?uni="+uni+"&carr="+carr+"&tab="+tab+"&mat="+mat,
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',

				success: function(json)
				{
					// Institucion
					$("#sNuevoNombre").attr('value', json.nombre);
					$("#sNuevoAcronimo").attr('value', json.acronimo);
					$("#sNuevoDir").attr('value', json.direccion);
					$("#sNuevoPais").attr('value', json.pais);

					// Carrera
					$("#sNuevoCodigoCar").attr('value', carr);
					$("#sNuevoNombreCar").attr('value', json.nombreCarrera);

					// Materia
					$("#sNuevoCodigoMat").attr('value', mat);
					$("#sNuevoNombreMat").attr('value', json.nombreMateria);
					$("#sNuevoCreditosMat").attr('value', json.creditosMateria);

					// Tabla

					$("#sUniSrc").attr('value', json.nombreUniv);
					$("#sCarreraSrc").attr('value', json.nombreCarreraSrc);
					$("#sMatSrc1").attr('value', json.materia1);
					$("#sMatSrc2").attr('value', json.materia2);
					$("#sMatSrc3").attr('value', json.materia3);

					$("#sUniDst").attr('value', json.nombre);
					$("#sCarreraDst").attr('value', json.nombreCarrera);
					$("#sMatDst1").attr('value', json.materia4);
					$("#sMatDst2").attr('value', json.materia5);
					$("#sMatDst3").attr('value', json.materia6);


					$.getJSON('rellenarInputs.action', function(data)
					{
						$.each(data.materiaList,function(index, value)
						{
							alert("value " + value);
						});
					});
				}
			});
        }

		$("#selectCar").change(function()
		{
			carr = $("#selectCar").find(':selected').val();
			cargarDatos();
			$('#selectMat')[0].options.length = 0;
			$.ajax(
			{
				type: "POST",
				url: "cargarMaterias?uni="+uni+"&carr="+carr,
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(json)
				{
					$('#selectMat').append('<option value=1>Seleccione Materia</option>');
					$('#selectMat2').append('<option value=1>Seleccione Materia</option>');
					$('#selectMat3').append('<option value=1>Seleccione Materia</option>');
					$.each(json, function(i, value)
					{
						$.each(value, function(j, index)
						{
							$('#selectMat').append($('<option>').text(j).attr("value",index));
							$('#selectMat2').append($('<option>').text(j).attr("value",index));
							$('#selectMat3').append($('<option>').text(j).attr("value",index));
						});
					});
				}
			});
		});

		$("#selectMat").change(function()
		{
			mat = $("#selectMat").find(':selected').val();
			cargarDatos();
		});

		$("#selectCar2").change(function()
		{
			carr = $("#selectCar2").find(':selected').val();
			cargarDatos();
			$('#selectMat4')[0].options.length = 0;
			$('#selectMat5')[0].options.length = 0;
			$('#selectMat6')[0].options.length = 0;
			$.ajax(
			{
				type: "POST",
				url: "cargarMaterias?uni="+uni+"&carr="+carr,
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(json)
				{
                    $('#selectMat4').append('<option value=1>Seleccione Materia</option>');
					$('#selectMat5').append('<option value=1>Seleccione Materia</option>');
					$('#selectMat6').append('<option value=1>Seleccione Materia</option>');
					$.each(json, function(i, value)
					{
						$.each(value, function(j, index)
						{
							$('#selectMat4').append($('<option>').text(j).attr("value",index));
							$('#selectMat5').append($('<option>').text(j).attr("value",index));
							$('#selectMat6').append($('<option>').text(j).attr("value",index));
						});
					});
				}
			});
		});

		$("#selectIns3").change(function()
		{
			uni3 = $("#selectIns3").find(':selected').val();
			$('#selectCar3')[0].options.length = 0;
			$.ajax(
			{
				type: "POST",
				url: "cargarCarreras?uni="+uni3,
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(json)
				{
					$('#selectCar3').append('<option value=1>Seleccione Carrera</option>');
					$.each(json, function(i, value)
					{
						$.each(value, function(j, index)
						{
							$('#selectCar3').append($('<option>').text(j).attr("value",index));
						});
					});
				}
			});
		});

		$("#selectCar3").change(function()
		{
			carr = $("#selectCar3").find(':selected').val();
			$('#selectTab')[0].options.length = 0;
			$.ajax(
			{
				type: "POST",
				url: "cargarTablas?uni="+uni3+"&carr="+carr,
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(json)
				{
					$('#selectTab').append('<option value=1>Seleccione Tabla de Equivalencia</option>');
					$.each(json, function(i, value)
					{
						$.each(value, function(j, index)
						{
							$('#selectTab').append($('<option>').text(j).attr("value",index));
						});
					});
				}
			});
		});


		$("#selectTab").change(function()
		{
			tab=$("#selectTab").find(':selected').val();
			cargarDatos();
		});
	});



