package beans;
import java.util.ArrayList;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import static com.opensymphony.xwork2.Action.ERROR;

/**
 *
 * @author brian-m
 */
public class Institucion
{
	private String nombre;
	private String acronimo;

	ArrayList<Institucion> institucionList = new ArrayList<Institucion>();
	ArrayList<Institucion> carreraList = new ArrayList<Institucion>();
	ArrayList<Institucion> tablaList = new ArrayList<Institucion>();
	private Map<String, String> instituciones = new HashMap<String, String>();

	public Map<String, String> getInstituciones()
	{
		reset();
		Connection con;
		Statement stmt;
		ResultSet res;
		
		try
		{
			Class.forName("org.postgresql.Driver").newInstance();
		}
		catch(Exception e)
		{
			return instituciones;
		}
		
		try
		{
			con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/equivalencia","sealos", "me");
			if (con == null)
				return null;

			stmt = con.createStatement();
			res = stmt.executeQuery("SELECT nombre, acronimo, dir, id_univ FROM UNIVERSIDAD ORDER BY nombre");
			while (res.next())
			{
				String univ = res.getString(1);
				String acro = res.getString(2);
				String dir = res.getString(3);
				String id = res.getString(4);
				instituciones.put(id, "("+acro+ ") "+univ+" " + "("+dir+")");
			}

			stmt.close();
			res.close();
			con.close();
		}
		catch (SQLException e)
		{
			System.out.println("Error accediendo base de datos: "+e);
		}

		return instituciones;
	}

	public void setInstituciones(Map<String, String> instituciones)
	{
		this.instituciones = instituciones;
	}

	public ArrayList getTablaList() {
		return tablaList;
	}

	public void setTablaList(ArrayList tablaList) {
		this.tablaList = tablaList;
	}

	public Institucion()
	{

	}

	public Institucion(String nom, String acron)
	{
		super();
		this.nombre = nom;
		this.acronimo = acron;
	}

	public String getNombre()
	{
		return nombre;
	}

	public void setNombre(String s)
	{
		nombre = s;
	}

	public String getAcronimo()
	{
		return acronimo;
	}

	public void setAcronimo(String a)
	{
		acronimo = a;
	}

	public ArrayList getCarreraList()
	{
		return carreraList;
	}

	public void setCarreraList(ArrayList a)
	{
		carreraList = a;
	}

	public ArrayList getInstitucionList()
	{
		reset();
		Connection con;
		Statement stmt;
		ResultSet res;
		
		try
		{
			Class.forName("org.postgresql.Driver").newInstance();
		}
		catch(Exception e)
		{
			return institucionList;
		}
		
		try
		{
			con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/equivalencia","sealos", "me");
			if (con == null)
				return null;

			stmt = con.createStatement();
			res = stmt.executeQuery("SELECT nombre, acronimo, dir, id_univ FROM UNIVERSIDAD ORDER BY nombre");
			while (res.next())
			{
				String univ = res.getString(1);
				String acro = res.getString(2);
				String dir = res.getString(3);
				String id = res.getString(4);
				institucionList.add(new Institucion(univ + " ("+acro+") " + "("+dir+")", id));
				
			}

			stmt.close();
			res.close();
			con.close();
		}
		catch (SQLException e)
		{
			System.out.println("Error accediendo base de datos: "+e);
		}

		return institucionList;
	}

	public void setInstitucionList(ArrayList a)
	{
		institucionList = a;
	}

	public void reset()
	{
		nombre = "";
		institucionList = new ArrayList();
		instituciones = new HashMap<String, String>();
	}
}
