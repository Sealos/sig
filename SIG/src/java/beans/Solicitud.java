package beans;

public class Solicitud
{
	private String nombre;
	private int nroexpediente;
	private String fecha;

	public String getFecha()
	{
		return fecha;
	}

	public void setFecha(String fecha)
	{
		this.fecha = fecha;
	}

	public int getNroexpediente()
	{
		return nroexpediente;
	}

	public void setNroexpediente(int nroexpediente)
	{
		this.nroexpediente = nroexpediente;
	}

	public Solicitud(String nom, int id, String fecha)
	{
		this.nombre = nom;
		this.nroexpediente = id;
		this.fecha = fecha;
	}

	public Solicitud() {}

	public String getNombre()
	{
		return nombre;
	}

	public void setNombre(String s)
	{
		nombre = s;
	}
}
