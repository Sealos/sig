package beans;
public class Login
{
	private String login;
	private String password;
	private String email;
	private String nombre;
	private int permiso;

	public int getPermiso()
	{
		return permiso;
	}

	public void setPermiso(int permiso)
	{
		this.permiso = permiso;
	}

	public String getLogin()
	{
		return login;
	}

	public void setLogin(String login)
	{
		this.login = login;
	}

	public String getNombre()
	{
		return nombre;
	}

	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}
	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}
}