INSERT INTO UNIVERSIDAD (acronimo, nombre, dir, pais) VALUES
('USB', 'Universidad Simón Bolívar', 'Caracas', 'Venezuela'),
('UNEFA', 'Uni. Nac. E. Pol. de la Fza. Armada Bol.', 'Puerto Ayacucho', 'Venezuela'),
('USM', 'Universidad Santa María', 'Puerto Ayacucho', 'Venezuela'),
('UNEFA', 'Uni. N. E. Pol. de la Fza. Armada Bol.', 'Puerto Píritu', 'Venezuela'),
('UNESR', 'Universidad Nac. E. Simón Rodríguez', 'Barcelona', 'Venezuela'),
('UGMA', 'Universidad Noror. G. M. de Ayacucho', 'Barcelona', 'Venezuela'),
('UNEFA', 'Uni. Nac. E. Pol. de la Fza. Armada Bol.', 'Maracay', 'Venezuela'),
('UNESR', 'Universidad Nac. E. Simón Rodríguez', 'Maracay', 'Venezuela'),
('UBV', 'Universidad Bolivariana de Venezuela', 'Maracay', 'Venezuela'),
('UBA', 'Universidad Bicentenaria de Aragua', 'Maracay', 'Venezuela'),
('UNELLEZ', 'Univ. N. E. de Llanos Occ. E. Zamora', 'Barinas', 'Venezuela'),
('UBV', 'Universidad Bolivariana de Venezuela', 'Barinas - Barinitas', 'Venezuela'),
('UNEFA', 'Uni. Nac. E. Pol. de la Fza. Armada Bol.', 'Barinas', 'Venezuela'),
('USM', 'Universidad Santa María', 'Barinas', 'Venezuela'),
('USI', 'Universidad Santa Inés', 'Barinas', 'Venezuela'),
('UNEG', 'Univ. Nac. Experimental de Guayana', 'Puerto Ordaz', 'Venezuela'),
('UBV', 'Universidad Bolivariana de Venezuela', 'Ciudad Bolívar - Caroní', 'Venezuela'),
('UNEFA', 'Uni. N. E. Pol. de la Fza. Armada Bol.', 'Ciudad Bolívar', 'Venezuela'),
('UNESR', 'Universidad Nac. E. Simón Rodríguez', 'Ciudad Bolívar', 'Venezuela'),
('UNEXPO', 'Universidad N. E. Poli. A. J. de Sucre', 'Puerto Ordaz', 'Venezuela'),
('UNEFA', 'Uni. Nac. E. Pol. de la Fza. Armada Bol.', 'Valencia - Puerto Cabello', 'Venezuela'),
('UNESR', 'Universidad Nac. E. Simón Rodríguez', 'Canoabo', 'Venezuela'),
('UC', 'Universidad de Carabobo', 'Naguanagua', 'Venezuela'),
('UNITEC', 'Universidad Tecnológica del Centro', 'Guacara - Valencia', 'Venezuela'),
('UJAP', 'Universidad José Antonio Páez', 'Valencia', 'Venezuela'),
('UAM', 'Universidad Arturo Michelena', 'Valencia', 'Venezuela'),
('UPV', 'Universidad Politécnica de Valencia', 'Valencia', 'Venezuela'),
('UNIPAP', 'Universidad Panamericana del Puerto', 'Puerto Cabello', 'Venezuela'),
('IVIC', 'Instituto Venezolano de Investigaciones Científicas', 'Miranda', 'Venezuela'),
('UCV', 'Universidad Central de Venezuela', 'Caracas', 'Venezuela'),
('UNESR', 'Universidad Nacional Experimental Simón Rodríguez', 'Caracas', 'Venezuela'),
('UPEL', 'Universidad Pedagógica Experimental Libertador', 'Caracas', 'Venezuela'),
('UNEFA', 'Universidad Nac. E. Politécnica de la Fza. Armada Bol.', 'Caracas', 'Venezuela'),
('UNA', 'Universidad Nacional Abierta', 'Caracas', 'Venezuela'),
('UBV', 'Universidad Bolivariana de Venezuela', 'Caracas', 'Venezuela'),
('UNEXPO', 'Universidad N. E. Politécnica A. J. de Sucre', 'Caracas', 'Venezuela'),
('UNIMET', 'Universidad Metropolitana', 'Caracas', 'Venezuela'),
('UCAB', 'Universidad Católica Andrés Bello', 'Caracas', 'Venezuela'),
('UAH', 'Universidad Alejandro Humboldt', 'Caracas', 'Venezuela'),
('UJMV', 'Universidad José María Vargas', 'Caracas', 'Venezuela'),
('LUV', 'La Universidad Virtual Latinoamericana', 'Caracas', 'Venezuela'),
('USM', 'Universidad Santa María', 'Caracas', 'Venezuela'),
('UMA', 'Universidad Monteávila', 'Caracas', 'Venezuela'),
('UCSAR', 'Universidad Católica Santa Rosa', 'Caracas', 'Venezuela'),
('IESA', 'Instituto de Estudios Sup. de Adm.', 'Caracas', 'Venezuela'),
('PSM', 'Instituto Universitario Politécnico Santiago Mariño', 'Caracas', 'Venezuela'),
('IUNP', 'Instituto Universitario de Nuevas Profesiones', 'Caracas', 'Venezuela'),
('IUTAV', 'Instituto Universitario Tec. Américo Vespucio', 'Caracas', 'Venezuela'),
('IUTIRLA', 'Instituto Universitario de Tec. Ind. R. L. Arismendi', 'Caracas', 'Venezuela'),
('ISUM', 'Instituto Superior Universitario de Mercadotecnia', 'Caracas', 'Venezuela'),
('UNEFM', 'Univ. Nac. Exp. Francisco de Miranda', 'Coro', 'Venezuela'),
('UNEFA', 'Uni. Nac. E. Pol. de la Fza. Armada Bol.', 'Coro', 'Venezuela'),
('UNESR', 'Universidad Nac. E. Simón Rodríguez', 'Coro', 'Venezuela'),
('UBV', 'Universidad Bolivariana de Venezuela', 'Carirubana', 'Venezuela'),
('UDEFA', 'Universidad de Falcón', 'Coro', 'Venezuela'),
('UNERG', 'Universidad N. E. de Llanos Centrales', 'San Juan de Los Morros', 'Venezuela'),
('UNEFA', 'Uni. Nac. E. Pol. de la Fza. Armada Bol.', 'El Socorro', 'Venezuela'),
('UNESR', 'Universidad Nac. E. Simón Rodríguez', 'San Juan de Los Morros', 'Venezuela'),
('UBV', 'Universidad Bolivariana de Venezuela', 'Valle de la Pascua', 'Venezuela'),
('UNEXPO', 'Univ. Nacional Exp. Politécnica', 'Barquisimeto', 'Venezuela'),
('UNEFA', 'Uni. Nac. E. Pol. de la Fza. Armada Bol.', 'Barquisimeto', 'Venezuela'),
('UNESR', 'Universidad Nac. E. Simón Rodríguez', 'Barquisimeto', 'Venezuela'),
('UCLA', 'Universidad Lisandro Alvarado', 'Barquisimeto', 'Venezuela'),
('UNEXPO', 'Universidad N. E. Poli. A. J. de Sucre', 'Barquisimeto', 'Venezuela'),
('UNY', 'Universidad Yacambú', 'Barquisimeto', 'Venezuela'),
('UFT', 'Universidad Fermín Toro', 'Cabudare', 'Venezuela'),
('ULA', 'Universidad de Los Andes', 'Mérida', 'Venezuela'),
('UNEFA', 'Uni. Nac. E. Pol. de la Fza. Armada Bol.', 'Mérida', 'Venezuela'),
('UNESR', 'Universidad Nac. E. Simón Rodríguez', 'El Vigía', 'Venezuela'),
('UNICA', 'Universidad Católica Cecilio Acosta', 'Mérida', 'Venezuela'),
('UNESR', 'Universidad Nac. E. Simón Rodríguez', 'Valles del Tuy - Los Teques', 'Venezuela'),
('UNEFA', 'Uni. Nac. E. Pol. de la Fza. Armada Bol.', 'Los Teques', 'Venezuela'),
('USM', 'Universidad Santa María', 'Sucre', 'Venezuela'),
('UNESR', 'Universidad Nac. E. Simón Rodríguez', 'Maturín', 'Venezuela'),
('UBV', 'Universidad Bolivariana de Venezuela', 'Maturín', 'Venezuela'),
('UNEFA', 'Uni. Nac. E. Pol. de la Fza. Armada Bol.', 'Maneiro', 'Venezuela'),
('UNE', 'Universidad Nueva Esparta', 'Nueva Esparta', 'Venezuela'),
('UNIMAR', 'Universidad de Margarita', 'García', 'Venezuela'),
('UNEFA', 'Uni. Nac. E. Pol. de la Fza. Armada Bol.', 'Guanare', 'Venezuela'),
('UNESR', 'Universidad Nac. E. Simón Rodríguez', 'Araure', 'Venezuela'),
('UDO', 'Universidad de Oriente', 'Cumaná', 'Venezuela'),
('UNEFA', 'Uni. Nac. E. Pol. de la Fza. Armada Bol.', 'Cumaná', 'Venezuela'),
('UNEFA', 'Uni. Nac. E. Pol. de la Fza. Armada Bol.', 'Betijoque', 'Venezuela'),
('UNESR', 'Universidad Nac. E. Simón Rodríguez', 'Valera', 'Venezuela'),
('UVM', 'Universidad Valle del Momboy', 'Valera', 'Venezuela'),
('UBV', 'Universidad Bolivariana de Venezuela', 'San Cristóbal - La Grita', 'Venezuela'),
('UNEFA', 'Uni. Nac. E. Pol. de la Fza. Armada Bol.', 'San Cristóbal', 'Venezuela'),
('UNESR', 'Universidad Nac. E. Simón Rodríguez', 'La Grita', 'Venezuela'),
('UNET', 'Universidad Nac. Exp. del Táchira', 'San Cristóbal', 'Venezuela'),
('IUTAI', 'Inst. Univ. de Tecnología Agro Industrial', 'San Cristóbal', 'Venezuela'),
('UCAT', 'Universidad Católica del Táchira', 'San Cristóbal', 'Venezuela'),
('UMC', 'Universidad Nac. Exp. Marítima del Caribe', 'Urimare', 'Venezuela'),
('UNEFA', 'Uni. Nac. E. Pol. de la Fza. Armada Bol.', 'La Guaira', 'Venezuela'),
('UNEY', 'Universidad Nac. Exp. del Yaracuy', 'San Felipe', 'Venezuela'),
('UNEFA', 'Uni. Nac. E. Pol. de la Fza. Armada Bol.', 'San Felipe', 'Venezuela'),
('UNERMB', 'Univ. Nac. Exp. Rafael María Baralt', 'Cabimas', 'Venezuela'),
('LUZ', 'Universidad del Zulia', 'Maracaibo', 'Venezuela'),
('UNESUR', 'Universidad Nac. Exp. Sur del Lago', 'Colón', 'Venezuela'),
('UBV', 'Universidad Bolivariana de Venezuela', 'Maracaibo', 'Venezuela'),
('UNEFA', 'Uni. Nac. E. Pol. de la Fza. Armada Bol.', 'Maracaibo', 'Venezuela'),
('URBE', 'Universidad Rafael Belloso Chacín', 'Maracaibo', 'Venezuela'),
('UNICA', 'Universidad Católica Cecilio Acosta', 'Maracaibo', 'Venezuela'),
('URU', 'Universidad Rafael Urdaneta', 'Maracaibo', 'Venezuela'),
('UNIOJEDA', 'Universidad Alonso de Ojeda', 'Lagunillas', 'Venezuela'),
('UJGH', 'Universidad Dr. José Gregorio Hernández', 'Maracaibo', 'Venezuela'),
('INTEVEP', 'Instituto Tecnológico Venezolano del Petróleo', 'Miranda', 'Venezuela'),
('HUC', 'Hospital Universitario de Caracas', 'Caracas', 'Venezuela'),
('INIA', 'Instituto Nacional de Investigaciones Agropecuarias', 'Aragua', 'Venezuela'),
('IDEA', 'Instituto de Estudios Avanzados', 'Miranda', 'Venezuela'),
('CIDA', 'Centro de Investigaciones de Astronomía', 'Mérida', 'Venezuela'),
('CMDLT', 'Centro Médico Docente La Trinidad', 'Caracas', 'Venezuela');