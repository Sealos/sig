CREATE TABLE usuario
(
	login			VARCHAR(32)		NOT NULL,		-- Login del usuario
	nombre			VARCHAR(64)		NOT NULL,		-- Nombre del usuario
	password		CHAR(32)		NOT NULL,		-- Password
	email			VARCHAR(320)	NOT NULL,		-- Maximo 320 caracteres (usr(64)@domain(255))
	privilegios		INTEGER			NOT NULL,		-- Flags de roles

	CONSTRAINT PK_USUARIO
		PRIMARY KEY(login)
);

CREATE SEQUENCE id_univ;
CREATE TABLE universidad
(
	id_univ			INTEGER			NOT NULL  DEFAULT nextval('id_univ'),
	acronimo		VARCHAR(10)		NOT NULL,		-- Acronimo de la universidad (USB, UCV, UNEFA...)
	nombre			VARCHAR(100)	NOT NULL,		-- Nombre completo de la universidad
	dir				VARCHAR(200)	NOT NULL,		-- Direccion de la universidad
	pais			VARCHAR(32)		NOT NULL,		-- Pais de la universidad

	UNIQUE (id_univ),
	CONSTRAINT PK_UNIVERSIDAD
		PRIMARY KEY(id_univ)
);
ALTER SEQUENCE id_univ OWNED BY universidad.id_univ;

CREATE TABLE carrera
(
	codigocarrera	VARCHAR(10)		NOT NULL,		-- Codigo de la carrera en la universidad
	nombrecarrera	VARCHAR(50)		NOT NULL,		-- Nombre de la carrera
	id_univ			INTEGER			NOT NULL,		-- Acronimo de la universidad donde pertenece la carrera

	CONSTRAINT PK_CARRERA
		PRIMARY KEY(codigocarrera, id_univ),
	CONSTRAINT FK_CARRERA
		FOREIGN KEY(id_univ)
			REFERENCES universidad(id_univ) ON DELETE CASCADE
);

CREATE SEQUENCE nroexpediente;
CREATE TABLE solicitud
(
	nroexpediente	INTEGER			NOT NULL	DEFAULT nextval('nroexpediente'),
	nombre			VARCHAR(20)		NOT NULL,		-- Nombre del aspirante
	apellido		VARCHAR(40)		NOT NULL,		-- Apellido del aspirante
	lugarNacimiento	VARCHAR(100)	NOT NULL,
	fechaSolicitud	DATE			NOT NULL,
	esNacionalizado	VARCHAR(5)		NOT NULL,
	fechaNacimiento	DATE			NOT NULL,
	identificacion	VARCHAR(12)		NOT NULL,
	tipoDoc			CHAR(1)			NOT NULL,
	nacionalidad	VARCHAR(20)		NOT NULL,
	sexo			CHAR(1)			NOT NULL,
	eCivil			VARCHAR(10)		NOT NULL,
	telefono		VARCHAR(15)		NOT NULL,
	direccion		VARCHAR(60)		NOT NULL,
	codigoCarrera	VARCHAR(10)		NOT NULL,		-- Codigo de la carrera en la universidad
	direccionEsp	VARCHAR(60)		NOT NULL,
	personaEsp		VARCHAR(40)		NOT NULL,
	nexo			VARCHAR(20)		NOT NULL,
	telefonoEsp		VARCHAR(15)		NOT NULL,
	email			VARCHAR(320)	NOT NULL,		-- Maximo 320 caracteres (usr(64)@domain(255))			
	aprobada		INTEGER			NOT NULL	DEFAULT -1,

	UNIQUE (nroexpediente),
	CONSTRAINT PK_solicitud
		PRIMARY KEY(nroexpediente)
);
ALTER SEQUENCE nroexpediente OWNED BY solicitud.nroexpediente;

CREATE TABLE materia
(
	codigo			VARCHAR(10)		NOT NULL,		-- Codigo de la materia en la universidad
	nombremateria	VARCHAR(64)		NOT NULL,		-- Nombre de la materia  
	codigocarrera	VARCHAR(10)		NOT NULL,		-- Codigo de la carrera en la universidad
	nrocreditos		NUMERIC			NOT NULL,		-- Numero de creditos asignados a la materia
	id_univ			INTEGER			NOT NULL,		-- Acronimo de la universidad donde se dicta la materia
	login			VARCHAR(32)		NOT NULL,		-- Codigo de la carrena del coordinador que agrego la materia

	CONSTRAINT PK_MATERIA
		PRIMARY KEY(codigo, codigocarrera, id_univ),
	CONSTRAINT FK_MATERIA_UNIV
		FOREIGN KEY(id_univ) REFERENCES UNIVERSIDAD(id_univ) ON DELETE CASCADE,
	CONSTRAINT FK_MATERIA_CARR
		FOREIGN KEY(codigocarrera, id_univ) REFERENCES CARRERA(codigocarrera, id_univ) ON DELETE CASCADE,
	CONSTRAINT FK_MATERIA_COORD
		FOREIGN KEY(login) REFERENCES usuario(login)
);

CREATE TABLE conjMateria
(
	nombreConj		VARCHAR(32)		NOT NULL,		-- Nombre del conjunto
	login			VARCHAR(32)		NOT NULL,		-- Codigo de la carrena del coordinador que agrego el conjunto

	CONSTRAINT PK_CONJMATERIA
		PRIMARY KEY(nombreConj, login),
	CONSTRAINT FK_CONJMATERIA
		FOREIGN KEY(login) REFERENCES usuario(login) 
);

CREATE TABLE materiasIncl
(
	nombreConj		VARCHAR(32)		NOT NULL,		-- Nombre del conjunto
	login			VARCHAR(32)		NOT NULL,		-- Codigo de la carrena del coordinador que agrego el conjunto
	codigo			VARCHAR(10)		NOT NULL,		-- Codigo de la materia en la universidad
	codigocarrera	VARCHAR(10)		NOT NULL,		-- Codigo de la carrera en la universidad
	id_univ			INTEGER			NOT NULL,		-- Acronimo de la universidad donde se dicta la materia

	CONSTRAINT PK_MATERIASINCL
		PRIMARY KEY(nombreConj, login, codigo, codigocarrera, id_univ),
	CONSTRAINT FK_MATERIASINCL_CONJ
		FOREIGN KEY(nombreConj, login) REFERENCES conjMateria(nombreConj, login) ON DELETE CASCADE,
	CONSTRAINT FK_MATERIASINCL_MATERIA
		FOREIGN KEY(codigo, codigocarrera, id_univ) REFERENCES MATERIA(codigo, codigocarrera, id_univ) ON DELETE CASCADE
);

CREATE TABLE equivalencia
(
	nombreConj		VARCHAR(32)		NOT NULL,		-- Nombre del conjunto
	login			VARCHAR(32)		NOT NULL,		-- Codigo de la carrena del coordinador que agrego el conjunto
	codigo			VARCHAR(10)		NOT NULL,		-- Codigo de la materia en la universidad
	codigocarrera	VARCHAR(10)		NOT NULL,		-- Codigo de la carrera en la universidad
	id_univ			INTEGER			NOT NULL,		-- Acronimo de la universidad donde se dicta la materia

	CONSTRAINT PK_EQUIVALENCIA
		PRIMARY KEY(nombreConj, login, codigo, codigocarrera, id_univ),
	CONSTRAINT FK_EQUIVALENCIA_CONJ
		FOREIGN KEY(nombreConj, login) REFERENCES conjMateria(nombreConj, login) ON DELETE CASCADE,
	CONSTRAINT FK_EQUIVALENCIA_MATERIA
		FOREIGN KEY(codigo, codigocarrera, id_univ) REFERENCES MATERIA(codigo, codigocarrera, id_univ) ON DELETE CASCADE
);
