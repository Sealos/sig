package action;

import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import beans.Institucion;

public class CargarInstituciones extends ActionSupport implements RequestAware, ServletRequestAware, ServletResponseAware
{
	private Map<String, String> instituciones = new HashMap<String, String>();
	private Map<String, Object> request1;
	private HttpServletRequest request = null;
	private HttpServletResponse response = null;

	public CargarInstituciones()
	{
		instituciones = new HashMap<String, String>();
	}

	public Map<String, String> getInstituciones()
	{
		Institucion ins = new Institucion();
		instituciones = ins.getInstituciones();
		return instituciones;
	}

	public void setInstituciones(Map<String, String> instituciones)
	{
		this.instituciones = instituciones;
	}
	
	@Override
	public void setServletRequest(HttpServletRequest request)
	{
		this.request = request;
	}

	@Override
	public void setServletResponse(HttpServletResponse response)
	{
		this.response = response;
	}

	@Override
	public void setRequest(Map<String, Object> map)
	{
		this.request1 = map;
	}

	@Override
	public String execute() throws Exception
	{
		return SUCCESS;
	}
}
