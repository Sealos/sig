package action;

import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.INPUT;
import static com.opensymphony.xwork2.Action.SUCCESS;
import java.sql.*;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.RequestAware;

public class AprobarPlanillaAction extends ActionSupport implements RequestAware,ServletRequestAware, ServletResponseAware {

	private Map<String, Object> request1;
	private HttpServletRequest request=null;
	private HttpServletResponse response=null;

	@Override
	public void setServletRequest(HttpServletRequest request)
	{
		this.request = request;
	}

	@Override
	public void setServletResponse(HttpServletResponse response)
	{
		this.response = response;
	}

	@Override
	public void setRequest(Map<String, Object> map)
	{
		this.request1 = map;
	}

	public AprobarPlanillaAction()
	{
	}

	@Override
	public String execute() throws Exception
	{
		Connection con;
		try
		{
			Class.forName("org.postgresql.Driver").newInstance();
		}
		catch(ClassNotFoundException e)
		{
		response.sendRedirect("login.jsp?error="+"BadDB");
		return ERROR;
		}

		Class.forName("org.postgresql.Driver").newInstance();
		con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/equivalencia","sealos", "me");

		if (con != null)
		{
			PreparedStatement psdoLogin;

			String nro = request.getParameter("nro");
			String sValor = request.getParameter("aprobado");

			if (nro == null || sValor == null)
			{
				System.out.println("Valores nulos, AprobarPlanilla");
				return INPUT;
			}
			try
			{
				String sqlOption="UPDATE solicitud SET aprobada = ? WHERE nroexpediente = ?";

				psdoLogin=con.prepareStatement(sqlOption);
				psdoLogin.setInt(1, Integer.parseInt(sValor));
				psdoLogin.setInt(2, Integer.parseInt(nro));

				psdoLogin.executeUpdate();
				psdoLogin.close();
				return SUCCESS;
			}
			catch(Exception e)
			{
				System.out.println(e);
				return INPUT;
			}
		}

		return INPUT;
	}
}