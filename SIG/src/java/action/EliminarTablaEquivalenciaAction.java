package action;

import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.INPUT;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.*;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.RequestAware;
import beans.Login;

public class EliminarTablaEquivalenciaAction extends ActionSupport implements RequestAware, ServletRequestAware, ServletResponseAware
{

	private HttpServletRequest request=null;
	private HttpServletResponse response=null;
	private Map<String, Object> request1;

	@Override
	public void setServletRequest(HttpServletRequest request)
	{
		this.request = request;
	}

	@Override
	public void setServletResponse(HttpServletResponse response)
	{
		this.response = response;
	}

	@Override
	public void setRequest(Map<String, Object> map)
	{
		this.request1 = map;
	}

	public EliminarTablaEquivalenciaAction()
	{
	}

	@Override
	public String execute() throws Exception
	{
		Connection con = null;
		try
		{
			Class.forName("org.postgresql.Driver").newInstance();
		}
		catch(ClassNotFoundException e)
		{

				response.sendRedirect("login.jsp?error="+"BadDB");
				return ERROR;

		}

		Class.forName("org.postgresql.Driver").newInstance();
		con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/equivalencia","sealos", "me");

		if (con != null)
		{
			PreparedStatement psdoLogin;

			String sTab = request.getParameter("codTab");
			Login usr = (Login)request.getSession().getAttribute("Login");
			String login = usr.getLogin();
			
			if (sTab.equals("0"))
			{
				request1.put("mensaje", "Debe selecionar una tabla de equivalencia.");
				return INPUT;
			}
			try
			{
				String sqlOption="DELETE FROM materiasIncl WHERE nombreConj = ? AND login = ?";
				psdoLogin=con.prepareStatement(sqlOption);
				psdoLogin.setString(1,sTab);
				psdoLogin.setString(2,login);
				psdoLogin.executeUpdate();
				
				sqlOption="DELETE FROM equivalencia WHERE nombreConj = ? AND login = ?";
				psdoLogin=con.prepareStatement(sqlOption);
				psdoLogin.setString(1,sTab);
				psdoLogin.setString(2,login);
				psdoLogin.executeUpdate();
				
				sqlOption="DELETE FROM conjMateria WHERE nombreConj = ? AND login = ?";
				psdoLogin=con.prepareStatement(sqlOption);
				psdoLogin.setString(1,sTab);
				psdoLogin.setString(2,login);

				psdoLogin.executeUpdate();
				psdoLogin.close();
				request1.put("mensaje", "Eliminacion exitosa.");
				return SUCCESS;
			}
			catch(Exception e)
			{
				request1.put("mensaje",e);
				return INPUT;
			}
		}
		return INPUT;
	}
}
