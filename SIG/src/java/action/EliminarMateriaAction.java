package action;

import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.INPUT;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

public class EliminarMateriaAction extends ActionSupport implements RequestAware, ServletRequestAware, ServletResponseAware
{

	private HttpServletRequest request=null;
	private HttpServletResponse response=null;
	private Map<String, Object> request1;


	@Override
	public void setServletRequest(HttpServletRequest request)
	{
		this.request = request;
	}

	@Override
	public void setServletResponse(HttpServletResponse response)
	{
		this.response = response;
	}

	@Override
	public void setRequest(Map<String, Object> map)
	{
		this.request1 = map;
	}


	public EliminarMateriaAction()
	{
	}

	public String execute() throws Exception
	{
		try
		{
			Class.forName("org.postgresql.Driver").newInstance();
		}
		catch(ClassNotFoundException e)
		{
			response.sendRedirect("login.jsp?error="+"BadDB");
			return ERROR;
		}

		Class.forName("org.postgresql.Driver").newInstance();
		Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/equivalencia","sealos", "me");

		if (con != null)
		{
			PreparedStatement psdoLogin;
			String sAcronimo = request.getParameter("acronimo");
			String sCodigo = request.getParameter("codCar");
            String sCodMat= request.getParameter("codMat");

			try
			{
				String sqlOption="DELETE FROM conjMateria WHERE nombreConj IN (SELECT nombreConj FROM materiasIncl WHERE codigo = ? AND codigocarrera= ? AND id_univ = ?);";
				psdoLogin=con.prepareStatement(sqlOption);
				psdoLogin.setString(1,sCodMat);
				psdoLogin.setString(2,sCodigo);
				psdoLogin.setInt(3,Integer.parseInt(sAcronimo));

				psdoLogin.executeUpdate();
				
				sqlOption="DELETE FROM conjMateria WHERE nombreConj IN (SELECT nombreConj FROM equivalencia WHERE codigo = ? AND codigocarrera= ? AND id_univ = ?);";
				psdoLogin=con.prepareStatement(sqlOption);
				psdoLogin.setString(1,sCodMat);
				psdoLogin.setString(2,sCodigo);
				psdoLogin.setInt(3,Integer.parseInt(sAcronimo));

				psdoLogin.executeUpdate();
				
				sqlOption="DELETE FROM materia WHERE codigocarrera = ? AND id_univ = ? AND codigo= ?";
				psdoLogin=con.prepareStatement(sqlOption);
				psdoLogin.setString(1,sCodigo);
				psdoLogin.setInt(2,Integer.parseInt(sAcronimo));
                psdoLogin.setString(3,sCodMat);

				psdoLogin.executeUpdate();
				psdoLogin.close();
				request1.put("mensaje","Se elimino la materia ("+sCodMat+")"
											+ " de la carrera " + sCodigo );

				return SUCCESS;
			}
			catch(Exception e)
			{
				request1.put("mensaje",e);
				e.printStackTrace();
			}
		}

		return INPUT;
    }
}