package action;

import beans.Login;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;

public class RellenarInputsAction extends ActionSupport
{
	private String nombre;
	private String acronimo;
	private String direccion;
	private String nombreCarrera;
	private String pais;

	private String codigoMateria;
	private String nombreMateria;
	private String creditosMateria;
	
	private String materia1;
	private String materia2;
	private String materia3;
	private String materia4;
	private String materia5;
	private String materia6;

	private String nombreUniv;
	private String nombreCarreraSrc;

	private Map<String, String> maps = new HashMap<String, String>();
	private List<String> materiaList;

	HttpServletRequest request = ServletActionContext.getRequest();

	public RellenarInputsAction()
	{
		String univ = request.getParameter("uni");
		String codCar = request.getParameter("carr");
		String nomTab = request.getParameter("tab");
		String codMat = request.getParameter("mat");
		Login usr = (Login)request.getSession().getAttribute("Login");
		String login = usr.getLogin();
		ResultSet res2;
		PreparedStatement psdoLogin;
		if (nomTab != null && !nomTab.equals("undefined"))
		{
			univ = null;
			codCar = null;
			codMat = null;
			try
			{
				Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/equivalencia","sealos", "me");

				// Obtenemos las universidades pertinentes
				psdoLogin=con.prepareStatement("SELECT e.id_univ, m.id_univ, e.codigo, m.codigo, e.codigocarrera, m.codigocarrera FROM conjMateria as c, equivalencia as e, materiasIncl as m WHERE c.login = ? AND c.nombreConj = ? AND c.login = e.login AND c.login = m.login AND c.nombreConj = e.nombreConj AND c.nombreConj = m.nombreConj");
				psdoLogin.setString(1, login);
				psdoLogin.setString(2, nomTab);
				res2 = psdoLogin.executeQuery();
				if (res2.next())
				{
					univ = res2.getString(1);
					codCar = res2.getString(5);
				}

				// Universidad de origen
				psdoLogin=con.prepareStatement("SELECT codigo, codigocarrera, id_univ FROM materiasIncl WHERE login = ? AND nombreConj = ?");
				psdoLogin.setString(1, login);
				psdoLogin.setString(2, nomTab);
				res2 = psdoLogin.executeQuery();

				String uSrc = "0";
				String cSrc = "0";

				int i = 0;
				while (res2.next())
				{
					switch(i)
					{
						case 0:
							uSrc = res2.getString(3);
							this.setMateria1(res2.getString(1));
							cSrc = res2.getString(2);
							break;
						case 1:
							this.setMateria2(res2.getString(1));
							break;
						case 2:
							this.setMateria3(res2.getString(1));
							break;
					}
					i++;
				}
			}
			catch (SQLException e)
			{
				System.out.println("Error accediendo base de datos: "+e);
				e.printStackTrace();
			}
		}

		if (univ != null && !univ.equals("undefined"))
		{
			try
			{
				Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/equivalencia","sealos", "me");
				psdoLogin = con.prepareStatement("SELECT nombre, acronimo, dir, pais FROM UNIVERSIDAD WHERE id_univ = " + "\'"+ univ +"\'");
				res2 = psdoLogin.executeQuery();

				while (res2.next())
				{
					this.setNombre(res2.getString(1));
					this.setAcronimo(res2.getString(2));
					this.setDireccion(res2.getString(3));
					this.setPais(res2.getString(4));
				}
				con.close();
			}
			catch (SQLException e)
			{
				System.out.println("Error accediendo base de datos: "+e);
				e.printStackTrace();
			}
		}

		if (codCar != null && !codCar.equals("undefined"))
		{
			try
			{
				Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/equivalencia","sealos", "me");

				psdoLogin=con.prepareStatement("SELECT nombrecarrera FROM CARRERA WHERE id_univ = " + "\'"+ univ +"\' AND codigocarrera= " + "\'"+ codCar +"\'");
				res2 = psdoLogin.executeQuery();

				while (res2.next())
				{
					this.setNombreCarrera(res2.getString(1));
				}
				con.close();
			}
			catch (SQLException e)
			{
				System.out.println("Error accediendo base de datos: "+e);
				e.printStackTrace();
			}
		}
		if (codMat != null && !codMat.equals("undefined"))
		{
			try
			{
				Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/equivalencia","sealos", "me");

				psdoLogin=con.prepareStatement("SELECT codigo, nombreMateria, nrocreditos FROM MATERIA WHERE id_univ = ? AND codigocarrera= ? AND codigo = ?");
				psdoLogin.setInt(1, Integer.parseInt(univ));
				psdoLogin.setString(2, codCar);
				psdoLogin.setString(3, codMat);
				res2 = psdoLogin.executeQuery();

				while (res2.next())
				{
					this.setCodigoMateria(res2.getString(1));
					this.setNombreMateria(res2.getString(2));
					this.setCreditosMateria(res2.getString(3));
				}

				con.close();
			}
			catch (SQLException e)
			{
				System.out.println("Error accediendo base de datos: "+e);
				e.printStackTrace();
			}
		}

		if (nomTab != null)
		{
			try
			{
				Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/equivalencia","sealos", "me");

				// Universidad de origen
				psdoLogin=con.prepareStatement("SELECT codigo, codigocarrera, id_univ FROM materiasIncl WHERE login = ? AND nombreConj = ?");
				psdoLogin.setString(1, login);
				psdoLogin.setString(2, nomTab);
				res2 = psdoLogin.executeQuery();
				String uSrc = "0";
				String cSrc = "0";

				int i = 0;
				while (res2.next())
				{
					switch(i)
					{
						case 0:
							uSrc = res2.getString(3);
							this.setMateria1(res2.getString(1));
							cSrc = res2.getString(2);
							break;
						case 1:
							this.setMateria2(res2.getString(1));
							break;
						case 2:
							this.setMateria3(res2.getString(1));
							break;	
					}
					i++;
				}
				psdoLogin=con.prepareStatement("SELECT acronimo FROM UNIVERSIDAD WHERE id_univ = " + "\'"+ uSrc +"\'");
				res2 = psdoLogin.executeQuery();
				while (res2.next())
				{
					this.setNombreUniv(res2.getString(1));
				}
				psdoLogin=con.prepareStatement("SELECT nombrecarrera FROM CARRERA WHERE id_univ = " + "\'"+ uSrc +"\' AND codigocarrera= " + "\'"+ cSrc +"\'");
				res2 = psdoLogin.executeQuery();
				while (res2.next())
				{
					this.setNombreCarreraSrc(res2.getString(1));
				}
				
				// Universidad destino
				psdoLogin=con.prepareStatement("SELECT codigo FROM equivalencia WHERE login = ? AND nombreConj = ?");
				psdoLogin.setString(1, login);
				psdoLogin.setString(2, nomTab);
				res2 = psdoLogin.executeQuery();

				i = 0;
				while (res2.next())
				{
					switch(i)
					{
						case 0:
							this.setMateria4(res2.getString(1));
							break;
						case 1:
							this.setMateria5(res2.getString(1));
							break;
						case 2:
							this.setMateria6(res2.getString(1));
							break;
					}
					i++;
				}

				con.close();
			}
			catch (SQLException e)
			{
				System.out.println("Error accediendo base de datos: "+e);
				e.printStackTrace();
			}
		}
	}

	@Override
	public String execute()
	{
		return Action.SUCCESS;
	}

	public String getNombre()
	{
		return nombre;
	}

	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}

	public String getAcronimo()
	{
		return acronimo;
	}

	public void setAcronimo(String acronimo)
	{
		this.acronimo=acronimo;
	}

	public String getDireccion()
	{
		return direccion;
	}

	public void setDireccion(String direccion)
	{
		this.direccion = direccion;
	}

	public String getNombreCarrera()
	{
		return nombreCarrera;
	}

	public void setNombreCarrera(String nombreCarrera)
	{
		this.nombreCarrera = nombreCarrera;
	}

	public Map<String, String> getMaps()
	{
		return maps;
	}

	public void setMaps(Map<String, String> maps)
	{
		this.maps = maps;
	}

	public List getMateriaList()
	{
		return materiaList;
	}

	public void setMateriaList(List materiaList)
	{
		this.materiaList=materiaList;
	}

	public String getPais()
	{
		return pais;
	}

	public void setPais(String pais)
	{
		this.pais = pais;
	}
	public String getCodigoMateria()
	{
		return codigoMateria;
	}

	public void setCodigoMateria(String codigoMateria)
	{
		this.codigoMateria = codigoMateria;
	}

	public String getNombreMateria()
	{
		return nombreMateria;
	}

	public void setNombreMateria(String nombreMateria)
	{
		this.nombreMateria = nombreMateria;
	}

	public String getCreditosMateria()
	{
		return creditosMateria;
	}

	public void setCreditosMateria(String creditosMateria)
	{
		this.creditosMateria = creditosMateria;
	}

		public String getMateria1()
		{
		return materia1;
	}

	public void setMateria1(String materia1)
	{
		this.materia1 = materia1;
	}

	public String getMateria2()
	{
		return materia2;
	}

	public void setMateria2(String materia2)
	{
		this.materia2 = materia2;
	}

	public String getMateria3()
	{
		return materia3;
	}

	public void setMateria3(String materia3)
	{
		this.materia3 = materia3;
	}

	public String getMateria4()
	{
		return materia4;
	}

	public void setMateria4(String materia4)
	{
		this.materia4 = materia4;
	}

	public String getMateria5()
	{
		return materia5;
	}

	public void setMateria5(String materia5)
	{
		this.materia5 = materia5;
	}

	public String getMateria6()
	{
		return materia6;
	}

	public void setMateria6(String materia6)
	{
		this.materia6 = materia6;
	}

	public String getNombreUniv()
	{
		return nombreUniv;
	}

	public void setNombreUniv(String nombreUniv)
	{
		this.nombreUniv = nombreUniv;
	}

	public String getNombreCarreraSrc()
	{
		return nombreCarreraSrc;
	}

	public void setNombreCarreraSrc(String nombreCarreraSrc)
	{
		this.nombreCarreraSrc = nombreCarreraSrc;
	}
	
}
