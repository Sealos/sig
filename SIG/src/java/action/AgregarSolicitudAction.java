package action;

import org.apache.commons.io.FileUtils;
import static com.opensymphony.xwork2.Action.INPUT;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
 
import com.opensymphony.xwork2.ActionSupport;
import java.sql.ResultSet;
import java.util.Map;
import org.apache.struts2.interceptor.ServletRequestAware;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
 
public class AgregarSolicitudAction extends ActionSupport implements RequestAware, ServletRequestAware, ServletResponseAware
{
	private List<File> fileUpload = new ArrayList<File>();
	private List<String> fileUploadContentType = new ArrayList<String>();
	private List<String> fileUploadFileName = new ArrayList<String>();
	private Map<String, Object> request1;
	private HttpServletRequest request=null;
	private HttpServletResponse response=null;
 
	public List<File> getFileUpload()
	{
		return fileUpload;
	}
 
	public void setFileUpload(List<File> fileUpload)
	{
		this.fileUpload = fileUpload;
	}
 
	public List<String> getFileUploadContentType()
	{
		return fileUploadContentType;
	}
 
	public void setFileUploadContentType(List<String> fileUploadContentType)
	{
		this.fileUploadContentType = fileUploadContentType;
	}
 
	public List<String> getFileUploadFileName()
	{
		return fileUploadFileName;
	}
 
	public void setFileUploadFileName(List<String> fileUploadFileName)
	{
		this.fileUploadFileName = fileUploadFileName;
	}
 
	public String upload() throws Exception
	{
		try
		{
			Class.forName("org.postgresql.Driver").newInstance();
		}
		catch(ClassNotFoundException e)
		{
			response.sendRedirect("login.jsp?error="+"BadDB");
			return ERROR;
		}

		Class.forName("org.postgresql.Driver").newInstance();
		Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/equivalencia","sealos", "me");

		if (con != null)
		{
			PreparedStatement psdoLogin;
			String val[] = new String[18];
			val[0] = request.getParameter("apellido");
			val[1] = request.getParameter("nombre");
			val[2] = request.getParameter("nac");			// V o E
			val[3] = request.getParameter("cedula");
			val[4] = request.getParameter("nacionalidad");
			val[5] = request.getParameter("sNuevoPais");
			val[6] = request.getParameter("datepicker");		// Fecha: DD/MM/YYYY
			val[7] = request.getParameter("nacionalizado");	// on o null
			val[8] = request.getParameter("estado");			// S C V D
			val[9] = request.getParameter("sexo");			// M F
			val[10] = request.getParameter("direccion");
			val[11] = request.getParameter("telefono");
			val[12] = request.getParameter("direccion2");
			val[13] = request.getParameter("telefono2");
			val[14] = request.getParameter("personaEsp");
			val[15] = request.getParameter("nexo");
			val[16] = request.getParameter("codCar");
			val[17] = request.getParameter("email");

			if (fileUpload.size() < 6)
			{
				addActionError("Faltan documentos por subir.");
				return INPUT;
			}			
			
			int i;
			for (i = 0; i < val.length; i++)
			{
				switch(i)
				{
					case 7:
						if (val[i] != null)
							val[i] = "true";
						else
							val[i] = "false";
						break;
					default:
						if (val[i] == null)
						{
							addActionError("No deje ningún campo en blanco.");
							return INPUT;
						}
						break;
				}
			}

			for (i = 0; i < fileUploadContentType.size(); i++)
			{
				if (!fileUploadContentType.get(i).equals("application/pdf"))
				{
					addActionError("Sólo están permitidos archivos PDFs, intente nuevamente");
					return INPUT;
				}
			}

			try
			{
				// Agregamos los datos
				String query = "INSERT INTO solicitud(apellido, nombre, tipoDoc, identificacion, nacionalidad, lugarNacimiento, fechaNacimiento, esNacionalizado, eCivil, sexo, direccion, telefono, direccionEsp, telefonoEsp, personaEsp, nexo, fechaSolicitud, codigoCarrera, email) VALUES (?, ?, ?, ?, ?, ?, to_date(?, 'DD/MM/YYYY'), ?, ?, ?, ?, ?, ?, ?, ?, ?, current_date, ?, ?)";
				psdoLogin = con.prepareStatement(query);
				for (i = 0; i < val.length; i++)
				{
					psdoLogin.setString(i + 1, val[i]);
				}

				psdoLogin.executeUpdate();
				// Buscamos el numero de la solicitud
				query = "SELECT currval('nroexpediente')";
				psdoLogin=con.prepareStatement(query);
				ResultSet res2 = psdoLogin.executeQuery();
				String numeroSol = "0";
				if (res2.next())
					numeroSol = res2.getString(1);

				// Aqui trabajamos con los archivos
                                
				String filePath = request.getSession().getServletContext().getRealPath("/uploadedDocs");
				FileUtils.forceMkdir(new File(filePath));

				fileUpload.get(0).renameTo(new File(filePath + "\\" + "Cedula_" + numeroSol + getExtension(fileUploadContentType.get(0))));
				fileUpload.get(1).renameTo(new File(filePath + "\\" + "CertBC_" + numeroSol + getExtension(fileUploadContentType.get(1))));
				fileUpload.get(2).renameTo(new File(filePath + "\\" + "CalifCert_" + numeroSol + getExtension(fileUploadContentType.get(2))));
				fileUpload.get(3).renameTo(new File(filePath + "\\" + "ProgEst_" + numeroSol + getExtension(fileUploadContentType.get(3))));
				fileUpload.get(4).renameTo(new File(filePath + "\\" + "Recibo_" + numeroSol + getExtension(fileUploadContentType.get(4))));
				fileUpload.get(5).renameTo(new File(filePath + "\\" + "Foto_" + numeroSol + getExtension(fileUploadContentType.get(5))));

				psdoLogin.close();
				addActionError("Solicitud agregada correctamente");
				return SUCCESS;
			}
			catch(Exception e)
			{
				addActionError("Error al ingresar los datos, intente nuevamente.");
				System.out.println(e);
				return INPUT;
			}
		}

		return INPUT;
	}
 
	public String display()
	{
		return NONE;
	}
	
	public String getExtension(String s)
	{
		
		System.out.println(s);
		if (s.equals("image/png"))
			return ".png";
		else if (s.equals("image/jpeg"))
			return ".jpg";
		else if (s.equals("application/pdf"))
			return ".pdf";
		
		return ".tmp";
	}

	@Override
	public void setServletRequest(HttpServletRequest request)
	{
		this.request = request;
	}

	@Override
	public void setServletResponse(HttpServletResponse response)
	{
		this.response = response;
	}

	@Override
	public void setRequest(Map<String, Object> map)
	{
		this.request1 = map;
	}
}
