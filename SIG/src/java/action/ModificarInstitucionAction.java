package action;

import static com.opensymphony.xwork2.Action.INPUT;
import java.sql.*;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.RequestAware;

public class ModificarInstitucionAction extends ActionSupport implements RequestAware,ServletRequestAware, ServletResponseAware
{
	private Map<String, Object> request1;
	private HttpServletRequest request=null;
	private HttpServletResponse response=null;

	@Override
	public void setServletRequest(HttpServletRequest request)
	{
		this.request = request;
	}

	@Override
	public void setServletResponse(HttpServletResponse response)
	{
		this.response = response;
	}

	@Override
	public void setRequest(Map<String, Object> map)
	{
		this.request1 = map;
	}

	public ModificarInstitucionAction()
	{
	}

	@Override
	public String execute() throws Exception
	{
		Connection con;
		try
		{
			Class.forName("org.postgresql.Driver").newInstance();
		}
		catch(ClassNotFoundException e)
		{
		response.sendRedirect("login.jsp?error="+"BadDB");
		return ERROR;
		}

		Class.forName("org.postgresql.Driver").newInstance();
		con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/equivalencia","sealos", "me");

		if (con != null)
		{
			PreparedStatement psdoLogin;

			String sNombre = request.getParameter("sNuevoNombre");
			String sAcro = request.getParameter("sNuevoAcronimo");
			String sDir = request.getParameter("sNuevoDir");
			String sPais = request.getParameter("sNuevoPais");
			String id = request.getParameter("acronimo");

			if (sNombre == null || sAcro == null || sPais == null || sDir == null)
				return INPUT;
			
			if (id.equals("0"))
			{
				request1.put("mensaje", "Debe selecionar una institución.");
				return INPUT;
			}
			
			if (sNombre.trim().equals("") || sAcro.trim().equals("") || sPais.trim().equals("") || sDir.trim().equals(""))
			{
				request1.put("mensaje", "No deje ningún campo en blanco.");
				return INPUT;
			}

			try
			{
				String sqlOption="UPDATE universidad SET (nombre, acronimo, dir, pais) = (?, ?, ?, ?) WHERE id_univ = ?";

				psdoLogin=con.prepareStatement(sqlOption);
				psdoLogin.setString(1,sNombre);
				psdoLogin.setString(2,sAcro);
				psdoLogin.setString(3,sDir);
				psdoLogin.setString(4,sPais);
				psdoLogin.setInt(5,Integer.parseInt(id));

				psdoLogin.executeUpdate();
				psdoLogin.close();
				request1.put("mensaje", "Modificacion exitosa de la universidad.");
				return SUCCESS;
			}
			catch(Exception e)
			{
				response.sendRedirect("modInstitucion.jsp?error="+"Ingrese un string no vacio");
				System.out.println(e);
				return INPUT;
			}
		}

		return INPUT;
	}
}