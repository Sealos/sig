package action;

import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.INPUT;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;

public class ConsultarPlanillaAction extends ActionSupport
{
	private String id;
	private String apellidos;
	private String nombres;
	private String tipoDoc;
	private String cedula;
	private String nacionalidad;
	private String pais;
	private String fechanac;
	private String nacionalizado;
	private String estadoCiv;
	private String sexo;
	private String email;
	private String direccion;
	private String telefono;
	private String direccion2;
	private String telefono2;
	private String personaEsp;
	private String nexo;
	private String carrera;
	private String fechaSolicitud;
	private String edad;

	public String getEdad()
	{
		return edad;
	}

	public void setEdad(String edad)
	{
		this.edad = edad;
	}

	private Map<String, String> maps = new HashMap<String, String>();
	private List<String> materiaList;

	HttpServletRequest request = ServletActionContext.getRequest();

	public ConsultarPlanillaAction()
	{
	}

	@Override
	public String execute()
	{
		Connection con;
		PreparedStatement psdoLogin;
		ResultSet res2;
		try
		{
			Class.forName("org.postgresql.Driver").newInstance();
			con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/equivalencia","sealos", "me");
		}
		catch(Exception e)
		{
			return ERROR;
		}

		if (con != null)
		{
			try
			{
				// Obtenemos las universidades pertinentes
				String query =  "SELECT *, TO_CHAR(fechaSolicitud, 'DD/MM/YYYY'), TO_CHAR(fechaNacimiento, 'DD/MM/YYYY'), date_part('year',age(fechaSolicitud, fechaNacimiento)) FROM solicitud WHERE nroexpediente="+id;   
				psdoLogin=con.prepareStatement(query);
				res2 = psdoLogin.executeQuery();
				if (res2.next())
				{
					id = res2.getString(1);
					nombres = res2.getString(2);
					apellidos = res2.getString(3);
					pais = res2.getString(4);
					nacionalizado = res2.getString(6);
					if (nacionalizado.equals("true"))
						nacionalizado = "Si";
					else
						nacionalizado = "No";
					cedula = res2.getString(8);
					tipoDoc = res2.getString(9);
					nacionalidad = res2.getString(10);
					sexo = res2.getString(11);
					if (sexo.equals("M"))
						sexo = "Masculino";
					else
						sexo = "Femenino";
					
					estadoCiv = res2.getString(12);
					telefono = res2.getString(13);
					direccion = res2.getString(14);
					carrera = res2.getString(15);
					direccion2 = res2.getString(16);
					personaEsp = res2.getString(17);
					nexo = res2.getString(18);
					telefono2 = res2.getString(19);
					email = res2.getString(20);
					fechaSolicitud = res2.getString(22);
					fechanac = res2.getString(23);
					edad = res2.getString(24);
				}
				
				query =  "SELECT nombrecarrera FROM carrera WHERE id_univ = 1 AND codigocarrera='"+carrera+"'";   
				psdoLogin=con.prepareStatement(query);
				res2 = psdoLogin.executeQuery();

				if (res2.next())
				{
					carrera = "("+carrera+") " + res2.getString(1);
				}

				con.close();
				return SUCCESS;
			}
			catch(Exception e)
			{
				addActionError("Error al ingresar los datos, intente nuevamente.");
				System.out.println(e);
				return INPUT;
			}
		}

		return INPUT;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getApellidos()
	{
		return apellidos;
	}

	public void setApellidos(String apellidos)
	{
		this.apellidos = apellidos;
	}

	public String getNombres()
	{
		return nombres;
	}

	public void setNombres(String nombres)
	{
		this.nombres = nombres;
	}

	public String getTipoDoc()
	{
		return tipoDoc;
	}

	public void setTipoDoc(String tipoDoc)
	{
		this.tipoDoc = tipoDoc;
	}

	public String getCedula()
	{
		return cedula;
	}

	public void setCedula(String cedula)
	{
		this.cedula = cedula;
	}

	public String getNacionalidad()
	{
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad)
	{
		this.nacionalidad = nacionalidad;
	}

	public String getPais()
	{
		return pais;
	}

	public void setPais(String pais)
	{
		this.pais = pais;
	}

	public String getFechanac()
	{
		return fechanac;
	}

	public void setFechanac(String fechanac)
	{
		this.fechanac = fechanac;
	}

	public String getNacionalizado()
	{
		return nacionalizado;
	}

	public void setNacionalizado(String nacionalizado)
	{
		this.nacionalizado = nacionalizado;
	}

	public String getEstadoCiv()
	{
		return estadoCiv;
	}

	public void setEstadoCiv(String estadoCiv)
	{
		this.estadoCiv = estadoCiv;
	}

	public String getSexo()
	{
		return sexo;
	}

	public void setSexo(String sexo)
	{
		this.sexo = sexo;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getDireccion()
	{
		return direccion;
	}

	public void setDireccion(String direccion)
	{
		this.direccion = direccion;
	}

	public String getTelefono()
	{
		return telefono;
	}

	public void setTelefono(String telefono)
	{
		this.telefono = telefono;
	}

	public String getDireccion2()
	{
		return direccion2;
	}

	public void setDireccion2(String direccion2)
	{
		this.direccion2 = direccion2;
	}

	public String getTelefono2()
	{
		return telefono2;
	}

	public void setTelefono2(String telefono2)
	{
		this.telefono2 = telefono2;
	}

	public String getPersonaEsp()
	{
		return personaEsp;
	}

	public void setPersonaEsp(String personaEsp)
	{
		this.personaEsp = personaEsp;
	}

	public String getNexo()
	{
		return nexo;
	}

	public void setNexo(String nexo)
	{
		this.nexo = nexo;
	}

	public String getCarrera()
	{
		return carrera;
	}

	public void setCarrera(String carrera)
	{
		this.carrera = carrera;
	}

	public String getFechaSolicitud()
	{
		return fechaSolicitud;
	}

	public void setFechaSolicitud(String fechaSolicitud)
	{
		this.fechaSolicitud = fechaSolicitud;
	}

	public Map<String, String> getMaps()
	{
		return maps;
	}

	public void setMaps(Map<String, String> maps)
	{
		this.maps = maps;
	}

	public List<String> getMateriaList()
	{
		return materiaList;
	}

	public void setMateriaList(List<String> materiaList)
	{
		this.materiaList = materiaList;
	}
}