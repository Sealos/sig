package action;

import beans.Solicitud;
import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.INPUT;
import static com.opensymphony.xwork2.Action.SUCCESS;

import com.opensymphony.xwork2.ActionSupport;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.RequestAware;

public class ConsultarSolicitudAction extends ActionSupport implements RequestAware, ServletRequestAware, ServletResponseAware
{
	private Map<String, Object> request1;
	private HttpServletRequest request = null;
	private HttpServletResponse response = null;
	ArrayList<Solicitud> solicitudes = new ArrayList<Solicitud>();
	ArrayList<Solicitud> solicitudesAprobadas = new ArrayList<Solicitud>();
	ArrayList<Solicitud> solicitudesRechazadas = new ArrayList<Solicitud>();

	public ArrayList<Solicitud> getSolicitudesAprobadas()
	{
		return solicitudesAprobadas;
	}

	public void setSolicitudesAprobadas(ArrayList<Solicitud> solicitudesAprobadas)
	{
		this.solicitudesAprobadas = solicitudesAprobadas;
	}

	public ArrayList<Solicitud> getSolicitudesRechazadas()
	{
		return solicitudesRechazadas;
	}

	public void setSolicitudesRechazadas(ArrayList<Solicitud> solicitudesRechazadas)
	{
		this.solicitudesRechazadas = solicitudesRechazadas;
	}
	
	private void cargarSolicitudes()
	{
		solicitudes = new ArrayList<Solicitud>();
		solicitudesAprobadas = new ArrayList<Solicitud>();
		solicitudesRechazadas = new ArrayList<Solicitud>();
		Connection con;
		Statement stmt;
		ResultSet res;
		try
		{
			con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/equivalencia","sealos", "me");
			if (con == null)
				return;

			stmt = con.createStatement();
			res = stmt.executeQuery("SELECT nombre, apellido, nroexpediente, TO_CHAR(fechaSolicitud, 'DD/MM/YYYY'), aprobada FROM SOLICITUD ORDER BY nroexpediente");
			int aprobada = 0;
			while (res.next())
			{
				String nombre = res.getString(2) + ", " + res.getString(1);
				int id = Integer.parseInt(res.getString(3));
				String fecha = res.getString(4);
				Solicitud sol = new Solicitud(nombre, id, fecha);
				aprobada = Integer.parseInt(res.getString(5));
				switch(aprobada)
				{
					case -1:
						solicitudes.add(sol);
						break;
					case 0:
						solicitudesRechazadas.add(sol);
						break;
					case 1:
						solicitudesAprobadas.add(sol);
						break;
				}
			}

			stmt.close();
			res.close();
			con.close();
		}
		catch (SQLException e)
		{
			System.out.println("Error accediendo base de datos: \n"+e);
		}
	}

	public ArrayList<Solicitud> getSolicitudes()
	{
		return solicitudes;
	}

	public void setSolicitudes(ArrayList<Solicitud> solicitudes)
	{
		this.solicitudes = solicitudes;
	}

	@Override
	public void setServletRequest(HttpServletRequest request)
	{
		this.request = request;
	}

	@Override
	public void setServletResponse(HttpServletResponse response)
	{
		this.response = response;
	}

	@Override
	public void setRequest(Map<String, Object> map)
	{
		this.request1 = map;
	}

	@Override
	public String execute() throws Exception
	{
		cargarSolicitudes();
		return SUCCESS;
	}
}
