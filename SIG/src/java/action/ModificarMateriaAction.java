package action;

import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.INPUT;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

public class ModificarMateriaAction extends ActionSupport implements RequestAware, ServletRequestAware, ServletResponseAware{

	private Map<String, Object> request1;
	private HttpServletRequest request=null;
	private HttpServletResponse response=null;

	@Override
	public void setServletRequest(HttpServletRequest request)
	{
		this.request = request;
	}

	@Override
	public void setServletResponse(HttpServletResponse response)
	{
		this.response = response;
	}

	@Override
	public void setRequest(Map<String, Object> map)
	{
		this.request1 = map;
	}

	public ModificarMateriaAction()
	{
	}

	public String execute() throws Exception
	{
		Connection con = null;
		try
		{
			Class.forName("org.postgresql.Driver").newInstance();
		}
		catch(ClassNotFoundException e)
		{
			response.sendRedirect("login.jsp?error="+"BadDB");
			return ERROR;
		}

		Class.forName("org.postgresql.Driver").newInstance();
		con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/equivalencia","sealos", "me");

		if (con != null)
		{
			PreparedStatement psdoLogin;

			String sAcronimo = request.getParameter("acronimo");
			String sCodigo = request.getParameter("codCar");
			String sNuevoNombre = request.getParameter("sNuevoNombre");
			String sCodMat= request.getParameter("codMat");

			if (sNuevoNombre.trim().equals(""))
			{
				request1.put("mensaje","Ingrese un nombre no vacio" );
				return INPUT;
			}

			try
			{
				String sqlOption="UPDATE materia SET nombremateria = ? WHERE codigocarrera = ? AND id_univ = ? AND codigo= ?";

				psdoLogin=con.prepareStatement(sqlOption);
				psdoLogin.setString(1,sNuevoNombre);
				psdoLogin.setString(2,sCodigo);
				psdoLogin.setInt(3,Integer.parseInt(sAcronimo));
				psdoLogin.setString(4,sCodMat);

				psdoLogin.executeUpdate();
				psdoLogin.close();
				request1.put("mensaje", "Se modificó la materia correctamente");

				return SUCCESS;
			}
			catch(Exception e)
			{
				request1.put("mensaje", "Error al modificar la materia");
				e.printStackTrace();
			}
		}
		return INPUT;
    }
}