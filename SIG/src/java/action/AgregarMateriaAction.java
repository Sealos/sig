
package action;

import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.INPUT;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import beans.Login;

public class AgregarMateriaAction extends ActionSupport implements RequestAware, ServletRequestAware, ServletResponseAware 
{

	private Map<String, Object> request1;
	private HttpServletRequest request=null;
	private HttpServletResponse response=null;

	@Override
	public void setServletRequest(HttpServletRequest request)
	{
		this.request = request;
	}

	@Override
	public void setServletResponse(HttpServletResponse response)
	{
		this.response = response;
	}

	@Override
	public void setRequest(Map<String, Object> map)
	{
		this.request1 = map;
	}

	public AgregarMateriaAction() 
	{
	}

	public String execute() throws Exception
	{
		try
		{
			Class.forName("org.postgresql.Driver").newInstance();
		}
		catch(ClassNotFoundException e)
		{
			response.sendRedirect("login.jsp?error="+"BadDB");
			return ERROR;
		}
		Class.forName("org.postgresql.Driver").newInstance();
		Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/equivalencia","sealos", "me");

		if (con != null)
		{
			PreparedStatement psdoLogin;
			// Se piden los datos de la materia.
			String sCodigo = request.getParameter("sNuevoCodigo");
			String sNombre = request.getParameter("sNuevoNombre");
			String sCreditos = request.getParameter("sNuevoCreditos");
			String sAcronimo = request.getParameter("acronimo");
			String sCodCar = request.getParameter("codCar");

			Login usr = (Login)request.getSession().getAttribute("Login");
			String sCodCoord = usr.getLogin();

			// Verificamos que estos campos no esten vacios.
			if (sNombre == null || sCodigo == null || sCreditos == null )
				return INPUT;
			if (sNombre.trim().equals("") || sCodigo.trim().equals("") || sCreditos.trim().equals(""))
			{
				request1.put("mensaje", "No deje ningún campo en blanco.");
				return INPUT;
			}
			try
			{
				// Se genera el codigo para agregar en la tabla materia los datos ingresados por el usuario.
				String sqlOption="INSERT INTO materia (codigo, nombremateria, codigocarrera, nrocreditos,id_univ,login) VALUES (?, ?, ?, ?, ?, ?)";
				// Se completa el codigo.
				psdoLogin=con.prepareStatement(sqlOption);
				psdoLogin.setString(1,sCodigo);
				psdoLogin.setString(2,sNombre);
				psdoLogin.setString(3,sCodCar);
				psdoLogin.setInt(4,Integer.parseInt(sCreditos));
				psdoLogin.setInt(5,Integer.parseInt(sAcronimo));
				psdoLogin.setString(6,sCodCoord);

				// Y finalmente es ejecutado en la base de datos.
				psdoLogin.executeUpdate();
				psdoLogin.close();
				request1.put("mensaje","Se agrego la materia: ("+sCodigo+") "+sNombre);
				return SUCCESS;
			}
			catch(Exception e)
			{
				// En caso que exista duplicidad muestra un error.
				request1.put("mensaje","Error; Ya existe una materia con ése códgio");
				System.out.println(e);
				return INPUT;
			}
		}
		return INPUT;
	}
}