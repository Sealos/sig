package action;

import java.sql.*;
import com.opensymphony.xwork2.ActionSupport;
import beans.Login;
import java.util.ArrayList;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.RequestAware;

public class AgregarCarreraAction extends ActionSupport implements RequestAware, ServletRequestAware, ServletResponseAware
{

	private Map<String, Object> request1;
	private HttpServletRequest request=null;
	private HttpServletResponse response=null;
	ArrayList lista= new ArrayList();

	Login loginForm = new Login();

	public AgregarCarreraAction()
	{
	}

	@Override
	public void setServletRequest(HttpServletRequest request)
	{
		this.request = request;
	}

	@Override
	public void setServletResponse(HttpServletResponse response)
	{
		this.response = response;
	}

	@Override
	public void setRequest(Map<String, Object> map)
	{
		this.request1 = map;
	}

	@Override
	public String execute() throws Exception
	{
		try
		{
			Class.forName("org.postgresql.Driver").newInstance();
		}
		catch(ClassNotFoundException e)
		{
			response.sendRedirect("login.jsp?error="+"BadDB");
			return ERROR;
		}
		Class.forName("org.postgresql.Driver").newInstance();
		Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/equivalencia","sealos", "me");

		if (con != null)
		{
			PreparedStatement psdoLogin;
			// Tomamos los datos de la carrera que se quiere agregar ingresados por el usuario
			String sCodigo = request.getParameter("sCodigo");
			String sNombre = request.getParameter("sNombre");
			String sAcronimo = request.getParameter("acronimo");
			// Verifica que el usuario completo los campos codigo y nombre
			if (sNombre.trim().equals("") || sCodigo.trim().equals(""))
			{
				request1.put("mensaje", "Ingrese un string no vacio");
				return INPUT;
			}

			try
			{
				// Se crea el comando que se desea ejecutar en la base de datos
				String sqlOption="INSERT INTO carrera(codigocarrera, nombrecarrera, id_univ) VALUES (?, ?, ?)";

				psdoLogin=con.prepareStatement(sqlOption);
				// Se utilizan los datos obtenidos para el comando que se ejecutara en la base de datos.
				psdoLogin.setString(1,sCodigo);
				psdoLogin.setString(2,sNombre);
				psdoLogin.setInt(3,Integer.parseInt(sAcronimo));

				psdoLogin.executeUpdate();
				psdoLogin.close();
				request1.put("mensaje","Se agrego la carrera");
				return SUCCESS;
			}
			catch(Exception e)
			{
				// En caso que ya la carrera este en la base de datos
				request1.put("mensaje","Error; Ya existe una carrera con ése códgio");
				return INPUT;
			}
		}
		return ERROR;
	}
}