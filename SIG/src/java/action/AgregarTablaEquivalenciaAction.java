package action;

import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.INPUT;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import beans.Login;

public class AgregarTablaEquivalenciaAction extends ActionSupport implements RequestAware, ServletRequestAware, ServletResponseAware
{
	private Map<String, Object> request1;
	private HttpServletRequest request=null;
	private HttpServletResponse response=null;


	@Override
	public void setServletRequest(HttpServletRequest request)
	{
		this.request = request;
	}

	@Override
	public void setServletResponse(HttpServletResponse response)
	{
		this.response = response;
	}

	@Override
	public void setRequest(Map<String, Object> map)
	{
		this.request1 = map;
	}
	
    public AgregarTablaEquivalenciaAction()
	{
	}

	public String execute() throws Exception
	{
		try
		{
			Class.forName("org.postgresql.Driver").newInstance();
		}
		catch(ClassNotFoundException e)
		{
			// si no se logra
			response.sendRedirect("login.jsp?error="+"BadDB");
			return ERROR;
		}
		Class.forName("org.postgresql.Driver").newInstance();
		Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/equivalencia","sealos", "me");
		if (con != null)
		{
			// se piden los campos de la solicitud
			String nombre = request.getParameter("sNombreReq");
			int val = Integer.parseInt(request.getParameter("val"));
			int uniOrigen = Integer.parseInt(request.getParameter("acronimo"));
			int uniDestino = Integer.parseInt(request.getParameter("acronimo2"));
			String carreraOrigen = request.getParameter("codCar");
			String carreraDestino = request.getParameter("codCar2");
			String materiasOrigen[] = new String[3];
			materiasOrigen[0] = request.getParameter("codMat");
			materiasOrigen[1] = request.getParameter("codMat2");
			materiasOrigen[2] = request.getParameter("codMat3");
			String materiasDestino[] = new String[3];
			materiasDestino[0] = request.getParameter("codMat4");
			materiasDestino[1] = request.getParameter("codMat5");
			materiasDestino[2] = request.getParameter("codMat6");
			String sTab = request.getParameter("codTab");

			Login usr = (Login)request.getSession().getAttribute("Login");
			String login = usr.getLogin();

			try
			{
				int i;
				String sqlOption;
				PreparedStatement psdoLogin;

				// Si estamos en modTabla borramos antes de agregar
				if(val == 1)
				{
					// Generamos el comando que ejecutaremos en la base de datos 
					// Lo completamos con los campos del usuario y finalmente ejecutamos
					sqlOption="DELETE FROM materiasIncl WHERE nombreConj = ? AND login = ?";
					psdoLogin=con.prepareStatement(sqlOption);
					psdoLogin.setString(1,sTab);
					psdoLogin.setString(2,login);
					psdoLogin.executeUpdate();

					sqlOption="DELETE FROM equivalencia WHERE nombreConj = ? AND login = ?";
					psdoLogin=con.prepareStatement(sqlOption);
					psdoLogin.setString(1,sTab);
					psdoLogin.setString(2,login);
					psdoLogin.executeUpdate();

					sqlOption="DELETE FROM conjMateria WHERE nombreConj = ? AND login = ?";
					psdoLogin=con.prepareStatement(sqlOption);
					psdoLogin.setString(1,sTab);
					psdoLogin.setString(2,login);
					psdoLogin.executeUpdate();
					nombre = sTab;
				}
				
				// Creamos el conjunto de materias
				sqlOption="INSERT INTO conjMateria (nombreConj, login) VALUES (?, ?)";
				psdoLogin = con.prepareStatement(sqlOption);
				psdoLogin.setString(1, nombre);
				psdoLogin.setString(2, login);
				psdoLogin.executeUpdate();
				
				// Insertamos los valores en el conjunto
				for (i = 0; i < 3; i++)
				{
					if (!materiasOrigen[i].equals("1"))
					{
						sqlOption="INSERT INTO materiasIncl (nombreConj, login, codigo, codigocarrera, id_univ) VALUES (?, ?, ?, ?, ?)";
						psdoLogin = con.prepareStatement(sqlOption);
						psdoLogin.setString(1, nombre);
						psdoLogin.setString(2, login);
						psdoLogin.setString(3, materiasOrigen[i]);
						psdoLogin.setString(4, carreraOrigen);
						psdoLogin.setInt(5, uniOrigen);
						psdoLogin.executeUpdate();
					}
				}
				
				// Colocamos la equivalencia
				for (i = 0; i < 3; i++)
				{
					if (!materiasDestino[i].equals("1"))
					{
						sqlOption="INSERT INTO equivalencia (nombreConj, login, codigo, codigocarrera, id_univ) VALUES (?, ?, ?, ?, ?)";
						psdoLogin = con.prepareStatement(sqlOption);
						psdoLogin.setString(1, nombre);
						psdoLogin.setString(2, login);
						psdoLogin.setString(3, materiasDestino[i]);
						psdoLogin.setString(4, carreraDestino);
						psdoLogin.setInt(5, uniDestino);
						psdoLogin.executeUpdate();
					}
				}
				psdoLogin.close();
				if (val == 0)
					request1.put("mensaje","Se agrego la equivalencia");
				else
					request1.put("mensaje","Se modifico la equivalencia");
				return SUCCESS;
			}
			catch(Exception e)
			{
				request1.put("mensaje","Error; Ya existe una tabla con estos datos");
				System.out.println(e);
				return INPUT;
			}
		}
		return INPUT;
	}
}