package action;

import static com.opensymphony.xwork2.Action.INPUT;
import java.sql.*;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.RequestAware;

public class AgregarInstitucionAction extends ActionSupport implements RequestAware, ServletRequestAware, ServletResponseAware
{

	private Map<String, Object> request1;
	private HttpServletRequest request=null;
	private HttpServletResponse response=null;

	@Override
	public void setServletRequest(HttpServletRequest request)
	{
		this.request = request;
	}

	@Override
	public void setServletResponse(HttpServletResponse response)
	{
		this.response = response;
	}

	@Override
	public void setRequest(Map<String, Object> map)
	{
		this.request1 = map;
	}

	public AgregarInstitucionAction()
	{
	}

	@Override
	public String execute() throws Exception
	{
		try
		{
			Class.forName("org.postgresql.Driver").newInstance();
		}
		catch(ClassNotFoundException e)
		{
			response.sendRedirect("login.jsp?error="+"BadDB");
			return ERROR;
		}

		Class.forName("org.postgresql.Driver").newInstance();
		Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/equivalencia","sealos", "me");

		if (con != null)
		{
			PreparedStatement psdoLogin;
			// Se obtienen los datos introducidos por el usuario
			String sNombre = request.getParameter("sNuevoNombre");
			String sAcro = request.getParameter("sNuevoAcronimo");
			String sDir = request.getParameter("sNuevoDir");
			String sPais = request.getParameter("sNuevoPais");

			// Verificamos que los campos no esten vacios
			if (sNombre == null || sAcro == null || sPais == null || sDir == null)
				return INPUT;

			// Verificamos que los campos no esten vacios
			if (sNombre.trim().equals("") || sAcro.trim().equals("") || sPais.trim().equals("") || sDir.trim().equals(""))
			{
				request1.put("mensaje", "No deje ningún campo en blanco.");
				return INPUT;
			}

			try
			{
				// Se crea el comando que se desea ejecutar en la base de datos
				String sqlOption="INSERT INTO universidad (acronimo, nombre, dir, pais) VALUES(?, ?, ?, ?)";

				psdoLogin=con.prepareStatement(sqlOption);
				// Con los datos obtenidos de los campos completamos el comando.
				psdoLogin.setString(1,sAcro);
				psdoLogin.setString(2,sNombre);
				psdoLogin.setString(3,sDir);
				psdoLogin.setString(4,sPais);
				// Y finalmente lo ejecutamos en la base de datos.
				psdoLogin.executeUpdate();
				psdoLogin.close();

				request1.put("mensaje","Se agrego la institucion: ("+sAcro+") "+sNombre);
				return SUCCESS;
			}
			catch(Exception e)
			{
				// En caso que exista duplicidad en la Base de datos.
				request1.put("mensaje","Error al ingresar los datos, verifique que la universidad no exista y que los campos esten correctos");
				System.out.println(e);
				return INPUT;
			}
		}
		return INPUT;
	}
}