package action;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;

public class CargarMateriasAction extends ActionSupport
{
	private Map<String, String> maps = new HashMap<String, String>();
	HttpServletRequest request = ServletActionContext.getRequest();
    
    public CargarMateriasAction()
	{
		String codCar = request.getParameter("carr");
		String univ= request.getParameter("uni");
		ResultSet res2;
		PreparedStatement psdoLogin;
		try
		{
			Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/equivalencia","sealos", "me");

			psdoLogin=con.prepareStatement("SELECT codigo, nombremateria FROM materia WHERE id_univ =  "+ "\'"+ univ +"\'" + " AND codigocarrera= " + "\'"+ codCar +"\'");
			res2 = psdoLogin.executeQuery();

			while (res2.next())
			{
                                
				String codigo = res2.getString(1);
				String nombre = res2.getString(2);
				maps.put(codigo+ " " + nombre, codigo);
			}

			con.close();
		}
		catch (SQLException e)
		{
			System.out.println("Error accediendo base de datos:  "+e);
			e.printStackTrace();
		}
	}

	public String execute()
	{
		return Action.SUCCESS;
	}

	public Map<String, String> getMaps()
	{
		return maps;
	}

	public void setMaps(Map<String, String> maps)
	{
		this.maps = maps;
	}
}
