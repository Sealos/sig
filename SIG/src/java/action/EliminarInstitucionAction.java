package action;

import static com.opensymphony.xwork2.Action.INPUT;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.*;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.RequestAware;

public class EliminarInstitucionAction extends ActionSupport implements RequestAware, ServletRequestAware, ServletResponseAware
{
	private HttpServletRequest request=null;
	private HttpServletResponse response=null;
	private Map<String, Object> request1;
	//response.setContentType("text/html;charset=UTF-8");

	@Override
	public void setServletRequest(HttpServletRequest request)
	{
		this.request = request;
	}

	@Override
	public void setServletResponse(HttpServletResponse response)
	{
		this.response = response;
	}

	@Override
	public void setRequest(Map<String, Object> map)
	{
		this.request1 = map;
	}

	public EliminarInstitucionAction()
	{
	}

	@Override
	public String execute() throws Exception
	{
		Connection con = null;
		try
		{
			Class.forName("org.postgresql.Driver").newInstance();
		}
		catch(ClassNotFoundException e)
		{
			response.sendRedirect("login.jsp?error="+"BadDB");
			return ERROR;
		}

		Class.forName("org.postgresql.Driver").newInstance();
		con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/equivalencia","sealos", "me");

		if (con != null)
		{
			PreparedStatement psdoLogin;

			String sAcro = request.getParameter("acronimo");
			if (sAcro != null && sAcro.equals("0"))
			{
				request1.put("mensaje", "Debe selecionar una institución.");
				return INPUT;
			}
			try
			{
				String sqlOption="DELETE FROM conjMateria WHERE nombreConj IN (SELECT nombreConj FROM materiasIncl WHERE id_univ = ?);";
				psdoLogin=con.prepareStatement(sqlOption);
				psdoLogin.setInt(1,Integer.parseInt(sAcro));
				psdoLogin.executeUpdate();
				
				sqlOption="DELETE FROM conjMateria WHERE nombreConj IN (SELECT nombreConj FROM equivalencia WHERE id_univ = ?);";
				psdoLogin=con.prepareStatement(sqlOption);
				psdoLogin.setInt(1,Integer.parseInt(sAcro));
				psdoLogin.executeUpdate();
				
				sqlOption="DELETE FROM universidad WHERE id_univ = ?";
				psdoLogin=con.prepareStatement(sqlOption);
				psdoLogin.setInt(1,Integer.parseInt(sAcro));

				psdoLogin.executeUpdate();
				psdoLogin.close();
				request1.put("mensaje", "Eliminacion exitosa.");
				return SUCCESS;
			}
			catch(Exception e)
			{
				request1.put("mensaje", "Error eliminando");
				return INPUT;
			}
		}
		return INPUT;
	}
}