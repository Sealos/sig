package action;

import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.INPUT;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;

public class EliminarPlanillaAction extends ActionSupport
{
	private String id;
	private Map<String, String> maps = new HashMap<String, String>();
	private List<String> materiaList;

	HttpServletRequest request = ServletActionContext.getRequest();

	public EliminarPlanillaAction()
	{
	}

	@Override
	public String execute()
	{
		Connection con;
		PreparedStatement psdoLogin;
		ResultSet res2;
		try
		{
			Class.forName("org.postgresql.Driver").newInstance();
			con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/equivalencia","sealos", "me");
		}
		catch(Exception e)
		{
			return ERROR;
		}

		if (con != null)
		{
			try
			{
				String sqlOption="DELETE FROM solicitud WHERE nroexpediente = \'" + id+"\'";
				psdoLogin=con.prepareStatement(sqlOption);
				psdoLogin.executeUpdate();

				con.close();
				return SUCCESS;
			}
			catch(Exception e)
			{
				addActionError("Error al eliminar la planilla");
				System.out.println(e);
				return INPUT;
			}
		}

		return INPUT;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}
}