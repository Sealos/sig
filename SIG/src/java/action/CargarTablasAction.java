package action;

import beans.Login;
import java.sql.*;
import java.util.Map;
import java.util.HashMap;
import com.opensymphony.xwork2.Action;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;

public class CargarTablasAction
{
	private Map<String, String> maps = new HashMap<String, String>();
	HttpServletRequest request = ServletActionContext.getRequest();

	public CargarTablasAction() throws Exception
	{
		String univ = request.getParameter("uni");
        String codCar = request.getParameter("carr");
		String tabB = request.getParameter("tab");
		if (tabB != null)
		{
			ResultSet res2;
			PreparedStatement psdoLogin;
			try
			{
				Login usr = (Login)request.getSession().getAttribute("Login");
				String sCodCoord = usr.getLogin();
				Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/equivalencia","sealos", "me");
				psdoLogin=con.prepareStatement("SELECT nombreConj FROM conjMateria WHERE nombreConj LIKE \'%" + tabB+"%\' AND login = \'" + sCodCoord+"\'");
				res2 = psdoLogin.executeQuery();

				while (res2.next())
				{
					String codigo = res2.getString(1);
					maps.put(codigo, codigo);
				}

				con.close();
			}
			catch (SQLException e)
			{
				System.out.println("Error accediendo base de datos:  "+e);
				e.printStackTrace();
			}
		}

		if (univ != null && codCar != null)
		{
			ResultSet res2;
			PreparedStatement psdoLogin;
			try
			{
				Login usr = (Login)request.getSession().getAttribute("Login");
				String sCodCoord = usr.getLogin();
				Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/equivalencia","sealos", "me");
				psdoLogin=con.prepareStatement("SELECT nombreConj FROM equivalencia WHERE id_univ = " + "\'"+ univ +"\' AND codigocarrera= " + "\'"+ codCar +"\'");
				res2 = psdoLogin.executeQuery();

				while (res2.next())
				{
					String codigo = res2.getString(1);
					maps.put(codigo, codigo);
				}

				con.close();
			}
			catch (SQLException e)
			{
				System.out.println("Error accediendo base de datos:  "+e);
				e.printStackTrace();
			}
		}
	}

	public String execute()
	{
		return Action.SUCCESS;
	}

	public Map<String, String> getMaps()
	{
		return maps;
	}

	public void setMaps(Map<String, String> maps)
	{
		this.maps = maps;
	}
}
